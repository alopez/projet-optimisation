import numpy as np
from   parser import read_tsp_file
import city
import graph

import simplexe 
import logging
import mincut

import scipy.optimize as scopt

# 
# Prendre un graphe et 
# coder les contraintes 
# linéaires du voyageur de commerce
# sous la forme d'une matrice 
# 


# Calcul des sous-ensembles propres

def subsets(l):
    """ 
        Retourne l'ensemble de sous ensembles
        sous la forme d'une liste où les 
        sous ensembles sont triés par taille
    """
    if l == []:
        return [[]]
    else:
        a = l[0]
        lp = l[1:]
        sub = subsets (lp)
        return sub + [ [a] + subset for subset in sub ] 

def proper_subsets(l):
    """ 
        Retourne l'ensemble de sous ensembles
        sous la forme d'une liste où les 
        sous ensembles sont triés par taille
        en supprimant l'ensemble vide et l'ensemble entier
    """
    if l == []:
        return []
    else:
        return subsets(l)[1:-1]

def in_cut(s,e):
    """
        Determine si l'arete e est dans la
        coupe s (definie par une liste de sommets)
    """
    if (e[0] in s) != (e[1] in s): # != === XOR
        return True
    else:
        return False




def pl_simple(graph):
    """ 
        Retourne sous la forme d'un programme
        lineaire les contraintes du voyageur de commerce sans
        contraintes de sous-tour
        
        Le PL renvoye est de la forme :

        minimiser c^T x avec Ax = b, x >= 0

    """
    
    edges = graph.edges 
    
    def cost(e):
        return graph.distance(e[0],e[1])
    
    # Dimensions
    le  = len(edges)
    print(len(edges))        # le PL aura le "vraies variables" + le variables "slack"
    n   = 2*le              # taille de l'espace associe
    ver = len(graph.cities) # nombre de sommets
    m   = ver + le          # ver egalites des sommets + le inegalites "slack"
    
    # Vecteur objectif : poids des aretes + poids nul variables slack
    c = list (map (cost, edges)) + [ 0 for i in range (le) ]
    
    # Vecteur des egalites : 2 pour les sommets + 0 pour les "slack"
    b = []
    A = []

    # Contraintes de type (1)
    # quelque soit u, somme x_{uv} = 2
    for i in graph.cities_number ():
        ligne = [ 0 for j in range(n) ] 

        for j in graph.cities_number ():
            if i != j:
                e = graph.edge_number (i,j)
                ligne[e] = 1

        A.append (ligne)
        b.append (2)

    # Contraintes de type (3)
    # (contraintes de slack)
    # x_{uv} + x_{uv} ' = 1
    for (u,v) in edges: 
        ligne = [ 0 for j in range(n) ]

        e = graph.edge_number (u,v)

        ligne[e] = 1
        ligne[e + le] = 1

        A.append (ligne)
        b.append (1)

    return(A,b,c)
    
        
        

def pl_full(graph):
    """
        Retourne sous la forme d'un programme
        lineaire les contraintes du voyageur de commerce avec
        contraintes de sous-tour
        
        Le PL renvoye est de la forme :
        maximiser c^T x avec Ax = b, x >= 0
    """

    # Recuperer le PL sans les contraintes
    (AA,bb,cc) = pl_simple(graph)
    edges      = graph.edges
    
    # Anciennes dimensions
    le  = len(edges)        # le PL simple avait le "vraies variables" + le variables "slack"
    nn  = 2*le
    ver = len(graph.cities) # nombre de sommets
    mm  = ver + le          # ver egalites des sommets + le inegalites "slack"
    
    # Sous-ensembles propres
    ps  = proper_subsets([c.number for c in graph.cities])
    lps = len (ps)

    # Nouvelles dimensions
    n = nn +lps # taille de l'espace associe : ajour de "slack"
    m = mm + lps # une contrainte en plus pour chaque subset
    
    # Nouveau vecteur objectif
    c = np.zeros(n)
    for j in range(nn):
        c[j] = cc[j]
    
    # Nouveau vecteur des egalites
    b = np.zeros(m)
    for i in range(mm): # anciennes valeurs
        b[i] = bb[i]
    for i in range(mm,m):
        b[i] = 2    # nouvelles valeurs 2
        
    # Nouvelle matrice
    A = np.zeros((m,n))
    for i in range(mm):
        for j in range(nn):
            A[i][j] = AA[i][j]

    for i in range(mm,m):
        for j in range(le):
            if in_cut(ps[i-mm],edges[j]):
                A[i][j] = 1
        A[i][i-mm + nn] = -1 # variable "slack"
    
    return (A,b,c)
        

def convert_to_edges (graph,solution):
    """
        Transforme une solution 
        du problème linéaire en
        une liste d'arêtes avec 
        les poids associés 
    """

    edges = graph.edges
    return list (zip (edges, solution))


def convert_to_circuit (graph,solution):
    """
        Transforme la solution sous forme d'un 
        problème linéaire en une suite 
        de villes

        Retourne une liste d'arêtes à prendre 

        ATTENTION: crée un tour quelque soit 
        l'entrée
    """
    
    # Valeurs renvoyees dans le probleme lineaire
    edges_values = solution[:len(graph.edges)]
    edges      = graph.edges
    
    # Aretes + valeurs
    compte     = list(zip (edges_values, edges))
    
    start  = compte[0][1][0]
    last   = compte[0][1][1]
    chemin = [ start ] 

    logging.debug (compte)
    logging.debug ("first : {} | last : {}".format (start, last))

    while last != None:
        logging.debug ("Trouve un successeur pour : {}".format (last))
        # sélectionne les arêtes sortantes 
        # qui ne font pas des boucles 
        def good_edge (x):
            (_,(u,v)) = x
            return (u == last or v == last) and not (u in chemin or v in chemin)

        going_out = list(filter (good_edge, compte))
        if going_out == []:
            chemin.append (last)
            last = None
            continue

        _,(u,v) = max (going_out, key = lambda x: x[0])

        chemin.append (last)
        if u == last:
            last = v
        elif v == last:
            last = u

        if last == start:
            last = None


    logging.debug ("Chemin trouvé : {}".format (chemin))
    
    if len (chemin) == len (graph.cities):
        chemin.append(start) # retour au depart
        return zip (zip (chemin, chemin[1:]), [ 1 for i in chemin ])
    else:
        logging.warning ("Erreur ?! ... chemin = {}".format (chemin))
        raise ValueError ("pouet pouet")


