##
# Le fichier qui gère les tests
# pour les différentes méthodes 
# d'approximation 

from   parser    import read_tsp_file
import city
import graph

import heuristique
import algonaif
import approximation2
import logging

# TODO: le bouger de là !!
def convert_to_tour(graph,valedges):
    
    """
        Convertit une solution fractionnaire du PL
        en une solution entiere qui est necessairement
        un tour, en suivant l'heuristique de poids
    """

    iteration = 0
    
    n = len(graph.cities)
    valedges.sort(key = lambda x : x[1]) # Trier par valeurs
    
    parts = [] # morceaux du tour
    interieur = []
    
    while True:
        iteration += 1

        # print ("PARTS: {}".format (parts))
        # print ("INTERIEUR: {}".format(interieur))

        # arrs = [ (partie[i],partie[i+1]) 
                  # for partie in parts 
                  # for i in range (len(partie)-1) ]
        
        # graphe.draw(chem=arrs,filename="images/00{}_APPROX_{}_glouton.png".format (iteration,filename))

        e,v = valedges.pop()
        #~ print("e",e)
        if((e[0] in interieur) or (e[1] in interieur)):
            continue # on ne peut pas faire un tour avec ca
        nn = len(parts)
        # Recuperer le morceau tour adjacent
        i = 0
        for p in parts:
            if(p[0]==e[0]):
                ii = 0
                break
            elif(p[-1] == e[0]):
                ii = 1
                break
            i += 1
        j = 0
        for p in parts:
            if(p[0]==e[1]):
                jj = 0
                break
            elif(p[-1] == e[1]):
                jj = 1
                break
            j += 1
        if(i == j and j != nn):
            continue
        # Ajout aux bons endroits 
        if(i == nn):
            if(j == nn):
                parts.append([e[0],e[1]])
            else:
                if(jj == 0):
                    interieur.append(parts[j][0])
                    parts[j] = [e[0]] + parts[j]
                else:
                    interieur.append(parts[j][-1])
                    parts[j] = parts[j] + [e[0]]
        else:
            if(j == nn):
                if(ii == 0):
                    interieur.append(parts[i][0])
                    parts[i] = [e[1]] + parts[i]
                else:
                    interieur.append(parts[i][-1])
                    parts[i] = parts[i] + [e[1]]
            else:
                if(ii == 0):
                    if(jj == 1):
                        interieur.append(parts[j][-1])
                        interieur.append(parts[i][0])
                        parts[i] = parts[j] + parts[i]
                        parts.pop(j)
                    else:
                        parts[j].reverse()
                        interieur.append(parts[j][-1])
                        interieur.append(parts[i][0])
                        parts[i] = parts[j] + parts[i]
                        parts.pop(j)
                else:
                    if(jj == 1):
                        parts[i].reverse()
                        interieur.append(parts[j][-1])
                        interieur.append(parts[i][0])
                        parts[i] = parts[j] + parts[i]
                        parts.pop(j)
                    else:
                        interieur.append(parts[j][0])
                        interieur.append(parts[i][-1])
                        parts[i] = parts[i] + parts[j]
                        parts.pop(j)
        
        if len(interieur)==n-2:
            break
    l = list(parts[0])
    l.append(l[0])
    
    return(l)

logging.basicConfig(filename='approximations_run.log',
                    level=logging.DEBUG,
                    format='%(asctime)s [%(levelname)s] %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')

# Choix du fichier test
filename = "tsp225"
probleme = read_tsp_file ("problemes/{}.tsp".format (filename))
graphe = probleme["GRAPH"]

# SQUARE 
tour = convert_to_tour (graphe, [ (e, 1 / (graphe.distance (e[0],e[1])** 2 )) for e in graphe.edges ])

graphe.draw(chem=graphe.sommets_vers_aretes (tour),filename="images/00_APPROX_{}_glouton.png".format (filename))

print ("GLOUTON SQUARE: {}".format (graphe.cout_chemin (tour)))

tour = heuristique.heuristique (graphe,tour)

graphe.draw(chem=graphe.sommets_vers_aretes (tour),filename="images/00_APPROX_{}_glouton_2opt.png".format (filename))
print ("GLOUTON SQUARE + 2OPT : {}".format (graphe.cout_chemin (tour)))

# 2-Approximation
tour = approximation2.approx2(graphe)

print ("APPROX2 :", graphe.cout_chemin(tour))


graphe.draw(chem=graphe.sommets_vers_aretes (tour),filename="images/00_APPROX_{}_approx2.png".format (filename))

tour = heuristique.heuristique (graphe,tour)


print ("APPROX2 + 2OPT :", graphe.cout_chemin(tour))


graphe.draw(chem=graphe.sommets_vers_aretes (tour),filename="images/00_APPROX_{}_approx2_2opt.png".format (filename))
