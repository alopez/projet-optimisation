
# Algorithme pour la min cut 
# Edmonds-Karp


## Fonction importante : mincut(graph,C)

from   graph import Graph
import math
import numpy as np
from   parser import read_tsp_file
import networkx as nx

    

def mincut(graph, C):
    """
        Coupe minimum du graphe
        graph pondere par la matrice C
        
        Retourne un tuple (coupe_min, valeur_min)
        coupe_min sous forme d'une liste de sommets
    """
    n = len(graph.cities)
    assert(n==len(C))
    
    H = nx.Graph()
    H.add_nodes_from(range(n))
    
    for i in range(n):
        for j in range(i+1,n):
            H.add_edges_from([(i,j,{"weight" : C[i][j]})])
    
    G = H.to_undirected()

    # La coupe minimale c'est la coupe qui est minimale, on prend 
    # donc le minimum sur les coupes possibles 
    # en comparant les coupes via leur valeur
    value, partition =  nx.stoer_wagner(G)
    par = partition[0]
    l = [i for i in par]
    return(l,value)
  
  
if __name__ == '__main__':
    # Choix du fichier
    probleme = read_tsp_file ("problemes/a0test.tsp")

    ### Exemple
    graphe = probleme["GRAPH"]
    C = [[0,1,0,0,0],
        [1,0,3,3,3],
        [0,3,0,3,3],
        [0,3,3,0,3],
        [0,3,3,3,0]]

    cut, value = mincut(graphe, C)
    print(value)

    for i in cut:
        print (i)

