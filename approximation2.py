#!/usr/bin/env python3

# Lopez Aliaume & Douéneau Gaëtan 

from   parser    import read_tsp_file
import city
import itertools
import graph
import logging

def minimum_spanning_tree(graph):
    """
        Algorithme de Kruskal
        Input : un graphe de la classe Graphe
        Output : un arbre couvrant de poids minimal
        sous forme d'une liste d'arêtes entre villes
    """

    # Initialisaiton de la liste des aretes
    edges = graph.edges
    
    # Tri de la liste des aretes
    def cost(e):
        return graph.distance(*e)

    logging.debug ("Spanning tree : sorting edges...")
    edges.sort(key = cost, reverse =1) # car les pop se font au bout
    logging.debug ("Spanning tree : edges sorted")
        
    # Kruskal
    n          = graph._n
    forest     = []                            # liste des aretes de la foret
    components = list (graph.cities_number ()) # liste des composantes connexes de la foret
    i          = 0

    logging.debug ("Spanning tree : waiting...")

    while len(forest)<n-1:
        e = edges.pop()
        # verifier qu'on ne cree pas de cycle
        cc0 = components[e[0]]
        cc1 = components[e[1]]

        if(cc0 != cc1):
            forest.append(e)

            # Modification des composantes connexes
            for j in range(len(components)):
                if(components[j] == cc1):
                    components[j] = cc0

    print("Spanning tree : done")
    return forest
    
    
    
def approx2(graph):
    
    """ 
        Heuristique basee sur l'arbre couvrant de
        poids minimum : garantit un ratio d'approximation
        de 2 dans le cas ou l'inegalité triangulaire est verifiée
    """
    
    logging.debug ("Starting approximation")
    
    tree = minimum_spanning_tree(graph)

    
    # Initialisation du parcours
    tour = [0]
    pile = [0]

    
    # Parcours en profondeur de l'arbre
    # l'algo n'est valide que parce que c'est
    # un arbre
    logging.debug ("Search : waiting...")
    while pile != []: # tant qu'il reste quelque chose dans la pile 
        pere = pile[-1] # car c'est une pile
        
        # chercher une arete correspondante
        found = False
        def find (elems):
            for (k,e) in enumerate(elems):
                if e[0] == pere:
                    return (k,e[1])
                elif e[1] == pere:
                    return (k,e[0])
            return None

        trouve = find (tree)
        if trouve == None:
            pile.pop () # pas de fils 
        else:
            i,fils = trouve
            del tree[i]

            tour.append(fils)
            pile.append(fils)
            
    logging.debug ("Search : done")
    tour.append(0)                  # c'est un cycle
    logging.debug ("Approximation : done")

    return(tour)
    
# Tests
    



#==============================================#
#### TRAITEMENT MULTIPLE

if __name__ == '__main__':
    # initialisation du loggeur 
    logging.basicConfig(filename='approx2_run.log',
                        level=logging.DEBUG,
                        format='%(asctime)s [%(levelname)s] %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')

    # Choix du fichier test
    filename = "ch150"
    probleme = read_tsp_file ("problemes/{}.tsp".format (filename))
    graphe = probleme["GRAPH"]
    tour = approx2(graphe)

    print ("\n===========")
    print ("TOUR APPROCHE :", tour)
    print ("APPROX2 :", graphe.cout_chemin(tour))
    print ("===========\n")


    graphe.draw(chem=graphe.sommets_vers_aretes (tour),filename="images/{}_approx2.png".format (filename))

    # f = open ("resultats_approximation2.txt","w")
    # f.write ("\\begin{tabular}{c|c}\n")
    # f.write ("\\textbf{Fichier} & \\textbf{Résultat} \\\\ \\hline \\hline \n")

    # cars = ["pr76","pr107","pr124","pr136","pr144","pr152"]
    # for p in cars:
        # probleme = read_tsp_file("problemes/{}.tsp".format (p))
        # graphe = probleme["GRAPH"]

        # tour = approx2(graphe)
        # f.write ("{} & {} \\\\ \\hline \n".format (p, graphe.cout_chemin (tour)))

    # f.write ("\\end{tabular}")
    # f.close () 
