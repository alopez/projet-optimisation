# coding=utf8
import numpy as np

import scipy.optimize as scopt
import lineartools as lin

def vrai_simplexe(A,b,c):
    res = scopt.linprog(c=c, A_eq=A, b_eq=b, 
                        bounds=(0,None), method='simplex', callback=None,
                        options={ "maxiter" : 100000, "disp" : True})
    return res


def simplexe_base(A,b,c,permut):
    
    """
    Simplexe pour resoudre
    max c avec Ax = b, x>=0
    
    Sous reserve que :
    - b>=0
    - base admissible 
        permut(0..m-1) : variables dans la base initiale
        permut(m..n-1) : variables hors base initiale
        
        
    Retourne solution optimale, valeur optimale
    
    """
    
    # m: nombre de contraintes = taille d'une base
    # n: nombres d'inconnues du pb d'optimisation linéaire
    m,n = A.shape
    assert(m<n and c.shape[0] == n and b.shape[0] == m)
    assert(min(b)>=0)

    n00 = 0
    while True :
        n00 += 1
        #print("Nouvelle permutation",permut)
        
        # matrice et gain permutés
        Ap=np.column_stack([A[:,permut[i]] for i in range(n)])
        cp=np.array([c[permut[i]] for i in range(n)])
        AB = Ap[:,:m] # matrice dans la base
        cB = cp[:m]
        AN = Ap[:,m:] # matrice hors de la base 
        cN = cp[m:]
        
        # vérification que c'est bien une base
        # Note : calculer le determinant ne sert 
        # a rien, on aura une erreur dans tous les
        # cas
        if(np.linalg.det(AB) == 0):
            raise ValueError("Failure : matrice singuliere")
        print("step:",n00)
        #invAB=lin.invert(AB)    # bien mais trop lent
        invAB = np.linalg.inv(AB) # pas le choix
        
        #expression des variables de base en
        #fonctions des variables hors base
        # x_B = bB - ABN x_N
        ABN = np.dot(invAB,AN)
        bBN = np.dot(invAB,b)
        
        # coefficients du gain dans les variables hors base
        cBN = cN - np.dot(cB,ABN)
        
        # liste de ceux qui sont positifs
        NeB = [j for j in range(m,n) if cBN[j-m] > 0]
        
        if NeB == []:
            # si tout est <=0 alors on a atteint l'optimum
            break

        # sinon on trouve une variable entrante
        oe = min([permut[j] for j in NeB]) # regle de Bland, entrant
        for j in NeB:
            if(permut[j] == oe):
                jeB = j
        
        # liste des variables suscepitbles de s'annuler dans la base
        BcB =[j for j in range(m) if ABN[j,jeB - m] > 0]
        
        if BcB == []:
            # on peut descendre à l'infini sans annuler la base
            raise ValueError("Probleme non borne")
            
        # sinon on cherche les variables qui s'annulent en premier
        thetaB = min([bBN[j]/float(ABN[j,jeB - m]) for j in BcB])
        # premieres variables a s'annuler
        BsB = [j for j in BcB if (bBN[j]/float(ABN[j,jeB - m]) == thetaB)]
        
        # on trouve une variable sortante
        os = min([permut[j] for j in BsB])# regle de Bland, sortant
        
        # modification de la base
        for i in range(n):
            if(permut[i] == oe):
                permut[i] = os
            elif(permut[i] == os):
                permut[i] = oe
        
        #print(permut)
                
    # CALCUL DE LA SOLUTION
    # Non permutee
    xp=np.hstack((bBN,np.zeros(n-m)))
    # permutee
    x=np.zeros(n)
    for i in range(n):
        x[permut[i]]=xp[i]
    # renvoie la solution, le gain, et la base
    return x,np.dot(c,x),permut



def simplexe(A,b,c):
    
    """ Algorithme du simplexe
    pour minimiser Ax = b
    """
    A = np.copy(np.array(A, dtype = float))
    b = np.copy(np.array(b, dtype = float))
    c = np.copy(np.array(c, dtype = float))
    # Dimensions
    m,n = A.shape
    
    ##################
    #### PHASE 1
    ##################
    
    # Nouvelles dimensions
    mm = m # pas d'ajout de n contraintes
    nn = n + m # ajout de m variables
    
    # Nouveau vecteur objectif
    cc = np.zeros(nn)
    for j in range(n,nn):
        cc[j] = -1
    
    # Nouveau vecteur des egalites
    bb = np.copy(b)
        
    # Nouvelle matrice
    AA = np.zeros((mm,nn))
    for i in range(m):
        for j in range(n):
            AA[i][j] = A[i][j] # meme matrice
    for i in range(m):
        AA[i][i+n] = 1
        
    # Base associee : les dernieres variables
    l = [i for i in range(nn)]
    l.reverse()
    permut_begin = np.array(l)

    # Appel de l'algo :
    sol1, opt1, permut = simplexe_base(AA,bb,cc,permut_begin)
    if(opt1 > 0):
        raise ValueError("Probleme infaisable")
    # Sinon opt1 = 0 donc les variables sont nulles
    
    ####################
    ####### PHASE 2
    ####################

    
    # On commence par verifier que les variables
    # artificielles sont bien sorties de la base,
    # sinon on les fait sortir manu militari
    
    #print("permut", permut)
    
    artificial = [i for i in range(m) if permut[i] > n-1] # variables artificielles
    for pivrow in artificial:
        # Matrices associees a la base
        Ap=np.column_stack((AA[:,permut[i]] for i in range(nn))) # matrice des colonnes permutées
        invAp=np.linalg.inv(Ap[:,:m]) # l'inverse de la matrice de la base
        turn=np.dot(invAp,Ap) # matrice "inversee"
        # Recherche d'un pivot possible
        # Il existe une vraie variable telle que la 
        # matrice "inversee" ait un coefficient non nul
        print(permut[pivrow])
        for pivcol in range(m,nn):
            print(pivcol, permut[pivcol],turn[pivrow,pivcol])
            if(permut[pivcol]<=n-1): # vraie variable
                if(turn[pivrow,pivcol] != 0):
                    break
        # Permutation des variables
        print(pivrow,pivcol)
        permut[pivrow], permut[pivcol] = permut[pivcol], permut[pivrow]
    
    # On se restreint au m premiers qui sont donc non-artificiels
    newpermut = list(permut[:m]) # nouvelle base
    
    for i in range(n):
        if not(i in newpermut):
            newpermut.append(i)
    newpermut = np.array(newpermut) # nouvelle permutation des n variables
    
    #print("newpermut",newpermut)

    sol, opt, permut = simplexe_base(A,b,-c,newpermut)
    opt = -opt
    
    return(sol,opt)


if __name__ == '__main__':

    A=np.array([[1,0,1,1,0,0,0],[0,1,2,0,1,0,0],[0,0,1,0,0,1,0],[4,6,2,0,0,0,1]])
    # second membre des contraintes Ax=b
    b=np.array([1000,1000,1500,8000])
    # coefficients de la fonction à maximiser
    c=np.array([4,12,3,0,0,0,0])
    # base initiale
    # permutation contenant les m variables dans la base (x>=0) puis
    # les variables hors base (n-m variables nulles)
    permut=np.array([1, 2, 4, 3, 5, 6, 0])
    print("============")
    print(vrai_simplexe(A,b,c))
    print("============")
    print(simplexe(A,b,c))

