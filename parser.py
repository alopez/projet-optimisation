from collections import deque
from city        import GeoCoord, GeoCity, Euc_2D, Explicit_2D
from graph       import Graph
import logging

def minimal_tsp():
    return { "COMMENT"          : ""
           , "DIMENSION"        : 0 
           , "TYPE"             : "" 
           , "EDGE_WEIGHT_TYPE" : ""
           , "EDGE_WEIGHT_FORMAT" : ""
           , "DISPLAY_DATA_TYPE" : ""
           , "GRAPH"            : None 
           , "MATRIX"           : None
           , "NODES"            : []
           , "DISPLAY"          : []
           }



# FAIRE UN PARSER BIEN MIEUX ... 
#
# parsing_func : tsp,tspfile -> Error | Result (a, tspfile) 
#
# ?!


def scan_keywords(tsp,tspfile):
    """
        Parse la configuration en début 
        de fichier

        Retourne la dernière ligne lue 
        qui n'est pas une ligne de configuration 
        globale 
    """
    for line in tspfile:
        words   = deque(line.split())
        
        keyword = words.popleft()
        value   = " ".join (words)

        if keyword[-1] != ":" and (value == "" or value[0] != ":"):
            return keyword + value 

        keyword = keyword.strip (": ")
        value   = value.strip (": ")
        tsp.setdefault(keyword, "")

        if keyword == "DIMENSION":
            tsp[keyword] = int (value)
        else:
            tsp[keyword] += value
    

def read_int(words):
    return int(words.popleft())
    
    
### Parsing des differents types de villes

def read_euc_2d_city(words, i):
    x = float(words.popleft())
    y = float(words.popleft())
    return Euc_2D(x, y, i)

def read_euc_3d_city(words, i):
    x = float(words.popleft())
    y = float(words.popleft())
    z = float(words.popleft())
    return Euc_3D(x, y, z, i)

def read_max_2d_city(words, i):
    x = float(words.popleft())
    y = float(words.popleft())
    return Max_2D(x, y, i)

def read_max_3d_city(words, i):
    x = float(words.popleft())
    y = float(words.popleft())
    z = float(words.popleft())
    return Max_3D(x, y, z, i)
    
def read_man_2d_city(words, i):
    x = float(words.popleft())
    y = float(words.popleft())
    return Man_2D(x, y, i)
    
def read_man_3d_city(words, i):
    x = float(words.popleft())
    y = float(words.popleft())
    z = float(words.popleft())
    return Man_3D(x, y, z, i)

def read_geo_coord(words):
    [degrees, minutes] = map(int, words.popleft().split("."))
    return GeoCoord(degrees, minutes)

def read_geo_city(words, i):
    lat = read_geo_coord(words)
    lon = read_geo_coord(words)
    return GeoCity(lat, lon, i)

def read_explicit_2d_city(words, i):
    x = float(words.popleft())
    y = float(words.popleft())
    return Explicit_2D(x, y, i)
    
def read_explicit_3d_city(words, i):
    x = float(words.popleft())
    y = float(words.popleft())
    z = float(words.popleft())
    return Explicit_3D(x, y, z, i)


def symetrise (matrix):
    """ Modifie la matrice pour 
        la rendre symétrique 
        dans le cas particulier
        où la matrice est donnée 
        via sa partie inférieure
        ou supérieure en mettant 
        des zéros ailleurs

        Comportement indéfini 
        dans le cas où la matrice 
        ne vérifie pas ces critères
    """
    N = len (matrix)


    for i in range (N):
        for j in range (N):
            if matrix[i][j] == 0:
                matrix[i][j] = matrix[j][i]
            else:
                matrix[j][i] = matrix[i][j]


def read_matrix (tsp, tspfile):
    """
        Lecture d'une matrice de poids
        dans le fichier de configuration

        Retourne un dictionnaire qui
        à (i,j) associe une valeur 
        potentiellement nulle

        le dictionnaire est défini 
        pour toute valeur de (i,j)
        raisonnable (dans les bornes)
    """

    # DOC
    # voilà ce qu'il se passe : on arrive 
    # dans un premier temps pour construire
    # une grosse séquence d'entiers qui codent la matrice
    # 1) Lire ligne par ligne et s'arrêter quand on ne peut plus lire un entier 
    # 2) Consturire la séquence qui est la concaténation des séquences 
    
    # Ensuite, il suffit de faire des choses intelligentes 
    # en construisant une séquence de « count » par type de matrice,
    # et en utilisant « interleave_generator » qui va utiliser 
    # cette séquence pour insérer les bonnes cases.

    # L'idée est la suivante : construire la matrice 
    # c'est faire la fusion de deux « streams »
    # 1) stream de valeurs 
    # 2) stream de clefs
    # 
    # Pour des raisons de simplicité, on a inliné 
    # la partie « fusion » et la partie génération 
    # du stream de clefs de manière partielle
    # ... voilà 


    N         = tsp["DIMENSION"]
    matrix    = [ [ 0 for j in range (N) ] for i in range (N) ] 
    entiers   = []
    last_line = None

    for line in tspfile:
        words = [x.strip (" ") for x in line.split () ]

        # essaie de voir si on peut 
        # lire un entier ... si oui c'est 
        # cool, sinon c'est pas cool et on 
        # retourne 
        try:
            int(words[0])
        except ValueError:
            last_line = line
            break

        entiers.append (int (x) for x in words)

    def sequential_generator (entiers):
        for l in entiers:
            for x in l:
                yield x

    def interleave_generator (sequence1, sequence2):
        i = 0
        j = 0
        count = next (sequence2) # the number of elems to read 
        for x in sequence1:
            if i == count:
                i =  0
                j += 1
                count = next (sequence2)
            yield ((i,j),x)
            i+=1

    if tsp["EDGE_WEIGHT_FORMAT"] == "UPPER_ROW":
        count_sequence = (N - i - 1 for i in range (N))
    elif tsp["EDGE_WEIGHT_FORMAT"] == "FULL_MATRIX":
        count_sequence = (N for i in range (N))
    elif tsp["EDGE_WEIGHT_FORMAT"] == "LOWER_ROW":
        count_sequence = (i for i in range (N))
    elif tsp["EDGE_WEIGHT_FORMAT"] == "UPPER_DIAG_ROW":
        count_sequence = (N - i  for i in range (N))
    elif tsp["EDGE_WEIGHT_FORMAT"] == "LOWER_DIAG_ROW":
        count_sequence = (i + 1 for i in range (N))
    else:
        raise ValueError ("Unkown matrix description")

    for ((j,i),v) in interleave_generator (sequential_generator (entiers), count_sequence):
        if tsp["EDGE_WEIGHT_FORMAT"] == "UPPER_ROW":
            matrix[i][i+j+1] = v
        elif tsp["EDGE_WEIGHT_FORMAT"] == "UPPER_DIAG_ROW":
            matrix[i][i+j] = v
        else:
            matrix[i][j] = v

    symetrise (matrix)

    return matrix, last_line



def read_cities(tsp,tspfile):

    cities = []
    logging.debug ("Reading {} lines from file".format (tsp["DIMENSION"]))

    for i in range(tsp["DIMENSION"]):

        line   = tspfile.readline()
        words  = deque(line.split()) # list of the words from the line
        
        assert i+1 == read_int(words) # check the number of the city is right

        if tsp["EDGE_WEIGHT_TYPE"] == "EUC_2D" or tsp["DISPLAY_DATA_TYPE"] == "TWOD_DISPLAY":
            cities.append(read_euc_2d_city(words, i))
        elif tsp["EDGE_WEIGHT_TYPE"] == "EUC_3D":
            cities.append(read_euc_3d_city(words, i))
        elif tsp["EDGE_WEIGHT_TYPE"] == "MAX_2D":
            cities.append(read_max_2d_city(words, i))
        elif tsp["EDGE_WEIGHT_TYPE"] == "MAX_3D":
            cities.append(read_max_3d_city(words, i))
        elif tsp["EDGE_WEIGHT_TYPE"] == "MAN_2D":
            cities.append(read_man_2d_city(words, i))
        elif tsp["EDGE_WEIGHT_TYPE"] == "MAN_3D":
            cities.append(read_man_3d_city(words, i))
        elif tsp["EDGE_WEIGHT_TYPE"] == "GEO":
            cities.append(read_geo_city(words, i))
        elif tsp["EDGE_WEIGHT_TYPE"] == "EXPLICIT":
            if tsp["DISPLAY_DATA_TYPE"] == "TWOD_DISPLAY" or tsp["NODE_COORD_TYPE"] == "TWOD_COORDS":
                cities.append(read_explicit_2d_city(words, i))
            elif tsp["NODE_COORD_TYPE"] == "TWOD_COORDS":
                cities.append(read_explicit_3d_city(words, i))
        else:
            logging.warning ("Unmatched coordinate type: " + tsp["EDGE_WEIGHT_TYPE"])

    return cities 



def read_tsp_file(path):
    tspfile = open(path,'r')
    tsp     = minimal_tsp()

    # Lit la configuration, et récupère 
    # la ligne juste en dessous 

    # Ceci est juste un problème chiant 
    # quand on lit une ligne, on passe à la suivante,
    # et donc on ne peut pas la « délire »
    #
    # Il faut donc s'en souvenir, parce que sinon on perd une 
    # information précieuse ... 
    # Sauf dans le cas de la lecture des cities, parce 
    # qu'on sait déjà combien de ligne on doit lire
    # à l'avance, et donc on peut ne pas lire 
    # une ligne « de trop » (qui permet de décider 
    # de stopper quand on ne sait pas combien 
    # de lignes lire)

    # autre remarque: on doit retirer les espaces, les tabulations 
    # et les fins de lignes parce que ces CONNARDS ne sont pas 
    # fichus d'avoir des fichiers cohérents entre eux ... 
    first_line = scan_keywords(tsp,tspfile).strip(" \n\t")

    while first_line != None and first_line != "":
        logging.debug (first_line)

        if first_line == "EDGE_WEIGHT_SECTION":
            tsp["MATRIX"],first_line  = read_matrix (tsp,tspfile)
            first_line = first_line.strip (" \n\t")
            continue

        elif first_line == "NODE_COORD_SECTION":
            tsp["NODES"]  = read_cities (tsp,tspfile)
        elif first_line == "DISPLAY_DATA_SECTION":
            tsp["DISPLAY"] = read_cities (tsp,tspfile)
        elif first_line == "EOF":
            break
        else:
            logging.warning ("Ne sait pas lire ... __{}__".format (first_line))
            first_line = None

        try:
            first_line = tspfile.readline ().strip (" \n\t")
        except:
            first_line = None 



    if tsp["EDGE_WEIGHT_TYPE"] != "EXPLICIT":
        g = Graph (tsp["NODES"], None)
    elif tsp["DISPLAY"] == []:
        # raise ValueError ("Matrice ... impossible à parser")
        g = Graph ([Explicit_2D  (number=i) for i in range (len (tsp["MATRIX"])) ], tsp["MATRIX"])
    else:
        # raise ValueError ("Matrice ... impossible à parser")
        g = Graph (tsp["DISPLAY"], tsp["MATRIX"])


    tsp["GRAPH"] = g

    tspfile.close()

    return tsp

if __name__ == '__main__':
    # initialisation du loggeur 
    logging.basicConfig(filename='parser_run.log',
                        level=logging.DEBUG,
                        format='%(asctime)s [%(levelname)s] %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')

    chemin = [ 1, 200, 198, 197, 195, 194, 218, 193, 196, 192, 191, 205, 189, 27, 188, 187, 186, 185, 184, 182, 181, 180, 179, 178, 177, 176, 174, 173, 172, 171, 170, 169, 168, 212, 214, 167, 166, 165, 164, 213, 158, 163, 162, 161, 160, 159, 157, 156, 155, 154, 153, 152, 151, 150, 149, 148, 147, 146, 145, 144, 143, 201, 142, 141, 140, 139, 138, 137, 136, 183, 135, 134, 215, 132, 131, 211, 130, 222, 129, 128, 127, 126, 125, 124, 123, 122, 121, 175, 120, 119, 118, 117, 116, 223, 115, 114, 113, 112, 111, 110, 109, 108, 107, 106, 105, 220, 104, 103, 102, 101, 100, 99, 98, 97, 96, 95, 209, 94, 93, 92, 91, 90, 89, 88, 87, 210, 86, 85, 84, 83, 82, 221, 81, 80, 79, 78, 77, 217, 219, 216, 76, 75, 74, 73, 72, 71, 70, 69, 68, 67, 66, 65, 64, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 50, 51, 49, 207, 2, 47, 225, 190, 133, 199, 224, 48, 45, 46, 44, 43, 42, 41, 40, 39, 38, 37, 36, 34, 33, 35, 32, 31, 206, 202, 30, 29, 28, 204, 26, 25, 208, 24, 23, 22, 21, 20, 203, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 1]

    filename = 'ts225'

    probleme = read_tsp_file ("problemes/{}.tsp".format(filename))
    graphe  = probleme["GRAPH"]

    chemin = graphe.sommets_vers_aretes ([ x - 1 for x in chemin ])

    graphe.draw (chem=chemin)


