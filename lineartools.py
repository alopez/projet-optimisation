import numpy as np

# Calcul de l'inverse d'une matrice
    
def invert(A):
    
    """ Inversion d'une matrice
    par la methode du pivot de Gauss
    
    """
    A = np.copy(np.array(A, dtype = float))
    m,n = A.shape
    assert(m==n)
    invA = np.eye(n,dtype = float)
    # Descente
    for y in range(n):
        # Recherche du pivot maximal
        maxrow = y
        for y2 in range(y+1, n):
            if abs(A[y2][y]) > abs(A[maxrow][y]):
                maxrow = y2
        # Pivot maximal en y
        T = np.copy(A[y,:])
        A[y,:] = np.copy(A[maxrow,:])
        A[maxrow, :] = T
        T = np.copy(invA[y,:])
        invA[y,:] = np.copy(invA[maxrow,:])
        invA[maxrow, :] = T
        # Verification
        if A[y][y] == 0:
            raise ValueError("Matrice singuliere")
        # Normalisation du pivot a 1
        c = A[y][y]
        for x in range(0,n):
            A[y][x] = A[y][x]/c
            invA[y][x] = invA[y][x]/c
        # Elimination des colonnes
        for y2 in range(y+1, n):
            p = A[y2][y]
            for x in range(0, n):
                A[y2][x] -= A[y][x]*p
                invA[y2][x] -= invA[y][x]*p
    # Remontee
    for y in range(n-1, 0-1, -1):
        c = A[y][y]
        # Mise a 0 de la colonne
        for y2 in range(y):
            d = A[y2][y]
            for x in range(n-1, 0-1, -1):
                A[y2][x] -=  (d/c) * A[y][x]
                invA[y2][x] -= (d/c) *invA[y][x]
        # Normalisation du pivot a 1
        for x in range(n):
            A[y][x] = A[y][x]/c
            invA[y][x] = invA[y][x]/c 
            
    return(invA)
    
if __name__ == '__main__':
    A=[[3.,3.,-1,11],[4.,5.,8,-12],[3.,3.,2,1],[0.,0.,0,2.]]
    print(invert(A))
    print(np.linalg.inv(A))
