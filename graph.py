import city
import pygame
import sys

import igraph

import logging

import random

def random_graph (n,maximum=1000):
    """
        Génère un graphe de type EUC_2D
        aléatoire avec n points
    """
    cities = [ city.Euc_2D (random.randrange (maximum), random.randrange(maximum), i)
               for i in range (n) ]

    logging.debug ("GENERATING EUC_2D CITIES :\n\t{}".format ("\n\t".join ([ "{},{}".format (u.x,u.y) for u in cities ])))

    return Graph (cities)


class Graph: 
    def __init__ (self, cities, weights=None):
        logging.debug ("Initialising GraphBase ...")

        self._graph = igraph.Graph.Full (len (cities))

        logging.debug ("Initialising graph ...")
        # List of the vertices as cities
        self.cities  = cities
        self._n      = len (cities)
        self._m      = self._n * (self._n - 1) / 2

        logging.debug ("Initialising weights")


        self.weights = [ None for edge in self._graph.es () ]
        if weights != None:
            for i,ligne in enumerate(weights):
                for j,e in enumerate(ligne):
                    print ("{:04d} ".format (e), end="")
                    if i != j:
                        self.weights[self.edge_number (i,j)] = e
                print ("")

        logging.debug ("Calculating edges")

        self.edges = list ((e.source, e.target) for e in self._graph.es () )


    def cities_number (self):
        """ 
            Parcours les numéros de villes 
            possibles dans le graphe 
        """
        for i in range (self._n):
            yield i


    def edges_number (self):
        """
            Parcours les numéros des edges
            possibles dans le graphe
        """

        for e in self._graph.es ():
            yield e.index 


    def mincut (self,C):
        """
            Retourne la coupe minimale 
            comme un objet

            mincut().value == valeur 
            mincut().cut   == sommets (nombres)

            C est le vecteur indexé par les 
            edges qui donne les capacités
        """

        cutted = self._graph.mincut (capacity = C)

        return (cutted.partition[0], cutted.value)

    def edge_number (self, city1, city2):
        """ 
            Récupère le numéro associé 
            à l'arête entre les deux villes 
            pour pouvoir l'utiliser plus tard

            self.edges[edge_number (i,j)] == (i,j)
        """
        if not (type(city1) is int):
            city1 = city1.number 

        if not (type(city2) is int):
            city2 = city2.number 

        return self._graph.get_eid (city1,city2)
        
    def distance (self, city1, city2):
        """ Calcule la distance entre deux villes 
            dans le graphe 

            Utilise la matrice de poids 
            ou la distance géographique
            en fonction des paramètres 
            de création de l'objet

            Les villes peuvent être 
            données comme des numéros 
            (en partant de ZÉRO)
            ou bien des des villes directement
            (de la classe ville)
        """
        if not (type(city1) is int):
            city1 = city1.number 

        if not (type(city2) is int):
            city2 = city2.number 

        en = self.edge_number (city1, city2)

        if self.weights[en] == None:
            self.weights[en] = self.cities[city1].distance(self.cities[city2])

        return self.weights[en]

    def sommets_vers_aretes (self, chemin):
        """ 
            Conversion d'un chemin sous 
            forme d'une liste de sommets 
            en un chemin sous forme 
            d'une liste d'arêtes 
        """
        def conv (x):
            a,b = x
            return (max (a,b), min (a,b))

        return list (map (conv, zip (chemin, chemin[1:])))

    def cout_chemin (self, chemin):
        """ Calcule le coût d'un chemin 
            comme la somme des distances 
            parcourues 

            (coût additif)
        """
        paires = zip (chemin, chemin[1:])

        return sum (self.distance (x,y) for (x,y) in paires)

    def cout_cycle (self, cycle):
        """ 
            Une fonction utile 
            pour calculer le coût d'un cycle 
            donné par seulement la liste 
            des noeuds du cycle
        """
        return self.cout_chemin (cycle + [cycle[0]])

    def draw(self, chem = None, poids = None, filename = None, maxL=1270, maxl = 670):
        """ 
            Dessine le graphe (si 2D)
            et dessine les arêtes données 
            en argument si pas None 
        """
        
        pygame.init()
        clock = pygame.time.Clock()

		# Colors
        WHITE = 255, 255, 255
        RED   = 255,   0,   0
        BLUE  =  50,   0, 200
        
        #Recuperer les coordonnees
        xx = [ city.x for city in self.cities ]
        yy = [ city.y for city in self.cities ]

        # Taille du dessin
        maxx, maxy = max(xx), max(yy)
        minx, miny = min(xx), min(yy)
        
        b     = (maxx - minx)/20
        sizex = maxx - minx + 2*b
        sizey = maxy - miny + 2*b
        # maxL  = 1270
        # maxl  = 670
        
        a = min ((maxL - 20)/float(sizex), (maxl - 20)/float(sizey))
        L = max (int(a*sizex),50)
        l = max (int(a*sizey),50)
        
        fenetre = pygame.display.set_mode((L, l))
        
        # Fonction de coordonnnee
        def coord(x,y):
            return (int(a*(x - minx+b)), int(a*(maxy - y +b)))
			
        # Boucle
        loop = True 

        # Création d'une image de la taille de la fenêtre
        background = pygame.Surface(fenetre.get_size())

        while loop:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    loop = False

            # Superposition du fond
            background.fill(WHITE)

            # Dessin des villes
            for i in range(len(self.cities)):
                pygame.draw.circle(background, RED, coord(xx[i],yy[i]), 2,2)

            # Dessin du chemin
            if chem != None:
                for (j,(u,v)) in enumerate (chem):
                    u = self.cities[u]
                    v = self.cities[v]
                    couleur = pygame.Color ("blue")
                    if poids != None:
                        c = int (200 * poids[j])
                        # print ("Couleur {} grace au poids {}".format (c, poids[j]))
                        couleur.r = 200 - c
                        couleur.b = 200 - c
                        couleur.g = 200 - c
                        if c > 0:
                            pygame.draw.line (background, couleur, coord (u.x,u.y), coord (v.x,v.y), 1)
                    else:
                        pygame.draw.line (background, couleur, coord (u.x,u.y), coord (v.x,v.y), 1)

            # Placement
            fenetre.blit(background, (0, 0))



            if filename != None:
                pygame.image.save (background, filename)
                loop = False
            else:
                # Rafraîchissement de l'écran
                pygame.display.flip()

                clock.tick(1000)
