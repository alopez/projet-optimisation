import numpy as np
from   parser import read_tsp_file
import city
import graph
import simplexe 
import logging
import mincut

import programmelineaire as pl

import scipy.optimize as scopt

def vrai_simplexe (A,b,c):
    AA = np.copy(np.array(A, dtype = float))
    bb = np.copy(np.array(b, dtype = float))
    cc = np.copy(np.array(c, dtype = float))
    res = scopt.linprog(c=cc, A_eq=AA, b_eq=bb, 
                        bounds=(0,None), method='simplex', callback=None,
                        options={ "maxiter" : 100000, "disp" : True})
    # logging.debug ("Résultat de l'appel à simplexe : {}".format (res))
    return res
    
def mon_simplexe (A,b,c):
    logging.debug ("Lancement du simplexe")
    res = simplexe.simplexe(A,b,c)
    logging.debug ("Le simplexe termine")
    return {"x":res[0],"fun":res[1]}



   
# La fonction à utiliser pour le simplexe
simplexe_func = vrai_simplexe

def in_cut(s,e):
    """
        Determine si l'arete e est dans la
        coupe s (definie par une liste de sommets)
    """
    if (e[0] in s) != (e[1] in s): # != === XOR
        return True
    else:
        return False

def algorithme_separation (graphe, filename="default_separation_output",epsilon = 0.0000001):
    """
        Effectue l'algorithme de la question 5 
        c'est à dire en ajoutant peu à peu 
        les contraintes de sous-tours 
        
        retourne la liste des arêtes avec 
        une valeur strictement positive 
    """
    (A,b,c)   = pl.pl_simple (graphe)
    edges     = graphe.edges
    iteration = 0

    while True:
        iteration += 1
        print("Iteration", iteration)
        logging.debug ("Résolution du problème pour la {}ème itération".format (iteration))

        # Récupère la valeur et la solution 
        res = simplexe_func (A,b,c)

        x = res["x"]
        logging.debug ("Solution [{}] :\n{}\n".format (res["fun"], x))
        
        sol = list(x[:len(edges)]) # seul le debut est important
        chemin = pl.convert_to_edges (graphe, sol)
        #~ graphe.draw(chem  = [u for (u,c) in chemin ], 
                   #~ poids = [c for (u,c) in chemin ], 
                   #~ filename="images/{}_separation_{}.png".format (filename, iteration))
        
        n = len(graphe.cities)
        C = np.zeros((n,n))
        for (poids,edge) in zip (sol,edges):
            C[edge[0]][edge[1]] = poids
            C[edge[1]][edge[0]] = poids # par symetrie
        
        coupe_min,valeur_min = mincut.mincut (graphe, C)

        if valeur_min < 2 - epsilon: # pallier les erreurs numeriques
            
            line = "\n\t".join ([ "({},{})".format (i,v) for (i,v) in enumerate (x[:len(edges)]) ])
            # logging.debug ("Valeur avant l'ajout de contrainte :\n\t{}".format (line))

            logging.debug ("Ajoute une contrainte de sous-tours correspondant à {}".format (coupe_min))
            
            # Anciennes valeurs
            AA = np.copy(A)
            bb = np.copy(b)
            cc = np.copy(c)
            
            mm = len(bb)
            nn = len(cc)
            
            # Nouveau vecteur objectif
            c = np.zeros(nn+1)
            for j in range(nn):
                c[j] = cc[j]
            
            # Nouveau vecteur des egalites
            b = np.zeros(mm+1)
            for i in range(mm): # anciennes valeurs
                b[i] = bb[i]
            b[mm] = 2    # nouvelle valeurs 2
                
            # Nouvelle matrice
            A = np.zeros((mm+1,nn+1))
            for i in range(mm):
                for j in range(nn):
                    A[i][j] = AA[i][j]

            for j in range(len(edges)):
                if in_cut(coupe_min,edges[j]):
                    A[mm][j] = 1
            A[mm][nn] = -1 # variable "slack"
            
        else:
            return pl.convert_to_edges (graphe, x)





def test_separation_on_random_graph ():
    """ 
        Un test de l'algorithme 
        de séparation pour un graphe aléatoire
    """
    graphe   = graph.random_graph (16)
    chemin   = algorithme_separation (graphe, "separation_random_graph")



if __name__ == '__main__':
    # initialisation du loggeur 
    logging.basicConfig(filename='separation_run.log',
                        level=logging.DEBUG,
                        format='%(asctime)s [%(levelname)s] :line %(lineno)d: (%(funcName)s) %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    
    test_separation_on_random_graph ()


    
