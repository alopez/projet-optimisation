# Lopez Aliaume & Douéneau Gaëtan 

from   parser    import read_tsp_file
import city
import graph
from   itertools import permutations
import random

import logging


def chemins (graph):
    l = permutations(list (graph.cities_number ()))
    return list(l)
    

def algorithme_naif (graph):
    """ Algorithme exact naïf """
    
    print("Starting naive exact algorithm")

    print("Minimization : waiting...")

    t_res = min (chemins (graph), key = graph.cout_cycle)

    print("Minimization : done")

    l_res = list (t_res)
    l_res.append (l_res[0])

    return l_res
