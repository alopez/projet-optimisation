# Un nouvel essai de code différent
###### EN CHANTIER :  SERA PEUT ETRE PRET POUR LA SOUTENANCE


import numpy as np
from   parser import read_tsp_file
import city
import graph
import simplexe 
import logging
import new_mincut
import random
import math
import cvxpy as cvx
# import cvxopt as cvx
import heuristique as h


def in_cut(s,e):
    """
        Determine si l'arete e est dans la
        coupe s (definie par une liste de sommets)
    """
    if (e[0] in s) != (e[1] in s): # != === XOR
        return True
    else:
        return False
        
        

def affiche(res):
    print("=============")
    print("Status",res["status"])
    print("Valeur opt", res["opt"])
    print("Valeur opt*", res["opt*"]) 
    #print("Vecteur opt", res["x"])
    print("=============")
    
    return


def minim(A_eq,b_eq, c, A_in = [], b_in = []):
    
    m,n = A_eq.shape
    assert(n == len(c) and (m==len(b_eq)))
    
    x = cvx.Variable(n)
    objective = cvx.Minimize(cvx.sum_entries(c*x))
    if(A_in == []):
        constraints = [0 <= x, x <= 1, A_eq*x == b_eq]
    else:
        constraints = [0 <= x, x <= 1, A_eq*x == b_eq, A_in*x >= b_in]
    
    prob = cvx.Problem(objective, constraints)
    opt = prob.solve()
    s = prob.status
    valx = np.copy(x.value)
    valx = np.array(np.transpose(valx)[0])
    
    return({"status":s,"opt":opt,"x":valx})
    
    
def random_tour(graph):
    """ 
        Un tour aleatoire
    """
    l = list (graph.cities_number ()) 
    random.shuffle (l) # NOTE: modifie la liste en place
    l.append (l[0])
    return(l)


def tour_to_feasible(graph,tour):
    v = len(graph.cities)
    x = np.zeros(len(graph.edges))
    
    for i in range(v): # tour de taille v+1
        c1 = tour[i]
        c2 = tour[i+1]
        e = graph.edge_number (c1,c2)
        x[e] = 1
        
    return(x)
    
    
def new_pl_simple(graph):
    """
        PL simple
        Le PL renvoye est de la forme :

        minimiser c^T x avec A_eq x = b_eq, x >= 0, x<=1

    """
    
    edges = graph.edges 
    
    def cost(e):
        return graph.distance(e[0],e[1])
    
    # Dimensions
    n = len(edges) # variables
    m = len(graph.cities) # contraintes
    
    # Vecteur objectif : poids des aretes
    c = np.array(list (map (cost, edges)),dtype = float)
    
    # Vecteur des egalites : 2 pour les sommets
    b_eq = np.array(2*np.ones(m),dtype = float)
    
    # Matrice des contraintes
    A_eq = []
    for i in graph.cities_number ():
        ligne = np.zeros(n)
        for j in graph.cities_number ():
            if i != j:
                e = graph.edge_number (i,j)
                ligne[e] = 1
        A_eq.append (ligne)
    A_eq = np.array(A_eq, dtype = float)

    return(A_eq,b_eq,c)





def convert_to_edges (graph,solution):
    """
        Transforme une solution 
        du problème linéaire en
        une liste d'arêtes avec 
        les poids associés 
    """

    edges = graph.edges
    return list (zip (edges, solution))



def convert_to_tour(graph,valedges):
    
    """
    Convertit une solution fractionnaire du PL
    en une solution entiere qui est necessairement
    un tour, en suivant l'heuristique de poids
    """
    
    n = len(graph.cities)
    valedges.sort(key = lambda x : x[1]) # Trier par valeurs
    
    parts = [] # morceaux du tour
    interieur = []
    
    while True:
        #~ print("======")
        #~ print(n)
        #~ print("int",len(interieur),interieur)
        #~ print("par",len(parts),parts)
        e,v = valedges.pop()
        #~ print("e",e)
        if((e[0] in interieur) or (e[1] in interieur)):
            continue # on ne peut pas faire un tour avec ca
        nn = len(parts)
        # Recuperer le morceau tour adjacent
        i = 0
        for p in parts:
            if(p[0]==e[0]):
                ii = 0
                break
            elif(p[-1] == e[0]):
                ii = 1
                break
            i += 1
        j = 0
        for p in parts:
            if(p[0]==e[1]):
                jj = 0
                break
            elif(p[-1] == e[1]):
                jj = 1
                break
            j += 1
        if(i == j and j != nn):
            continue
        # Ajout aux bons endroits 
        if(i == nn):
            if(j == nn):
                parts.append([e[0],e[1]])
            else:
                if(jj == 0):
                    interieur.append(parts[j][0])
                    parts[j] = [e[0]] + parts[j]
                else:
                    interieur.append(parts[j][-1])
                    parts[j] = parts[j] + [e[0]]
        else:
            if(j == nn):
                if(ii == 0):
                    interieur.append(parts[i][0])
                    parts[i] = [e[1]] + parts[i]
                else:
                    interieur.append(parts[i][-1])
                    parts[i] = parts[i] + [e[1]]
            else:
                if(ii == 0):
                    if(jj == 1):
                        interieur.append(parts[j][-1])
                        interieur.append(parts[i][0])
                        parts[i] = parts[j] + parts[i]
                        parts.pop(j)
                    else:
                        parts[j].reverse()
                        interieur.append(parts[j][-1])
                        interieur.append(parts[i][0])
                        parts[i] = parts[j] + parts[i]
                        parts.pop(j)
                else:
                    if(jj == 1):
                        parts[i].reverse()
                        interieur.append(parts[j][-1])
                        interieur.append(parts[i][0])
                        parts[i] = parts[j] + parts[i]
                        parts.pop(j)
                    else:
                        interieur.append(parts[j][0])
                        interieur.append(parts[i][-1])
                        parts[i] = parts[i] + parts[j]
                        parts.pop(j)
        
        if len(interieur)==n-2:
            break
    l = list(parts[0])
    l.append(l[0])
    
    return(l)
    


def algorithme_separation(graphe, filename="default_separation_output",epsilon = 0.0001):
    
    (A_eq,b_eq,c)   = new_pl_simple (graphe)
    A_in = []
    b_in = []
    edges     = graphe.edges
    n = len(edges)
    iteration = 0
    oldvalue = 0
    
    nb_slow = 0

    while True:
        iteration += 1
        print("\nIteration", iteration)
        
        res = minim(A_eq,b_eq,c,A_in,b_in)
        sol = res["x"]
        value = np.vdot(c,sol)

        # chemin_majorant = convert_to_tour(graphe,convert_to_edges (graphe,sol))
        # chemin_majorant = h.heuristique(graphe, chemin_majorant)
        res["opt*"] = 0 # graphe.cout_chemin (chemin_majorant)

        affiche(res)
        
        if(value-oldvalue<0.000001):
            nb_slow += 1
        else:
            nb_slow = 0
        
        oldvalue = value
        chemin = convert_to_edges (graphe, sol)
        
        if(iteration % 10 == 1):
            if(isinstance(graphe.cities[0],city.Euc_2D)):
                graphe.draw(chem  = [u for (u,c) in chemin ], 
                        poids = [c for (u,c) in chemin ], 
                        filename="images/0CVX_{}_simple_{}.png".format (filename,iteration))
        
        
        sol = np.round(sol,3)
        n0 = len(graphe.cities)
        C = np.zeros((n0,n0))
        for (poids,edge) in zip (sol,edges):
            C[edge[0]][edge[1]] = poids
            C[edge[1]][edge[0]] = poids # par symetrie

        # ORACLE DE SEPARATION
            
        coupe_min,valeur_min = new_mincut.mincut (graphe, C)
        #coupe_min, valeur_min = graphe.mincut ([ poids for (poids,edge) in zip (sol, edges) ])

        print("mincutted : {}".format (valeur_min))

        if ((valeur_min < 2 - epsilon) and (nb_slow)<20):
            
            A_in = list(A_in)
            b_in = list(b_in)
            
            b_in.append(2)
            l = np.zeros(n) # ligne de A_in associee a la derniere valeur
            for j in range(n):
                if in_cut(coupe_min,edges[j]):
                    l[j] = 1
            A_in.append(l)

            
            A_in = np.array(A_in)
            b_in = np.array(b_in)
            
        else:
            if(isinstance(graphe.cities[0],city.Euc_2D)):
                graphe.draw(chem  = [u for (u,c) in chemin ], 
                        poids = [c for (u,c) in chemin ], 
                        filename="images/0CVX_{}_separationfinie.png".format (filename))
            return value,convert_to_edges (graphe, sol)


def primal_dual (graphe, filename="primal_dual", epsilon=0.0001):
    pass


def test(filename):
    """ 
        Un test de l'algorithme 
        PL simple sur un fichier donné 
    """

    probleme = read_tsp_file ("problemes/{}.tsp".format(filename))

    graphe  = probleme["GRAPH"]

    # Construction du problème linéaire 
    (A_eq,b_eq,c) = new_pl_simple (graphe)
    
    # Minimisation
    tour = random_tour(graphe)
    x0 = tour_to_feasible(graphe, tour)
    res = minim(A_eq = A_eq, b_eq = b_eq, c =c)
    affiche(res)
    
    x = res["x"]

    chemin = convert_to_edges (graphe, x)
    graphe.draw(chem=[ u for (u,c) in chemin ], poids = [c for (u,c) in chemin ], filename="images/0CVX_{}_simple.png".format (filename))
    
    return
    
def test_separation_on_file (filename):
    """ 
        Un test de l'algorithme 
        de séparation pour un fichier donné
    """
    probleme = read_tsp_file ("problemes/{}.tsp".format(filename))
    graphe  = probleme["GRAPH"]
    value, edgesvalues   = algorithme_separation (graphe, filename)
    
    print("\nSEPARATION ENDED\n")
    
    c = convert_to_tour(graphe,edgesvalues)
    c = h.heuristique(graphe,c)
    edges_c = list(zip(c,c[1:]))
    if(isinstance(graphe.cities[0],city.Euc_2D)):
        graphe.draw(chem=edges_c, filename="images/0CVX_{}_approxPL.png".format (filename))
    sup = graphe.cout_chemin(c)
    inf = int(round(value))
    
    print("\n================\n================\n")
    print("RESULTATS")
    print("Borne superieure",sup)
    print("Borne inferieure",inf)
    if(sup == inf):
        print("Optimum atteint")
        if(isinstance(graphe.cities[0],city.Euc_2D)):
            graphe.draw(chem=edges_c, filename="images/0tour_optimal_{}.png".format (filename))
    print("\n================\n================\n")
    
    
    
test_separation_on_file("tsp225")
