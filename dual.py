# 
# Le dual du problème linéaire ...
# ici pour simplifier 
# 

import graph
import city
import logging
from   parser import read_tsp_file
import random


import programmelineaire as pl

import mincut
import numpy as np
import scipy.optimize as scopt

import cvxpy as cvx

def minim(A_eq,b_eq, c, A_in = [], b_in = []):
    
    A_eq = np.array (A_eq)
    b_eq = np.array (b_eq)
    c    = np.array (c)

    m,n = A_eq.shape
    assert(n == len(c) and (m==len(b_eq)))
    
    x = cvx.Variable(n)
    objective = cvx.Minimize(cvx.sum_entries(c*x))
    if(A_in == []):
        constraints = [0 <= x, A_eq*x == b_eq]
    else:
        constraints = [0 <= x, A_eq*x == b_eq, A_in*x >= b_in]
    
    prob = cvx.Problem(objective, constraints)
    opt = prob.solve()
    s = prob.status

    valx = np.copy(x.value)
    valx = np.array(np.transpose(valx)[0])
    
    return({"status":s,"opt":opt,"x":valx})

def subsets(l):
    """ 
        Retourne l'ensemble de sous ensembles
        sous la forme d'une liste où les 
        sous ensembles sont triés par taille
    """
    if l == []:
        return [[]]
    else:
        a = l[0]
        lp = l[1:]
        sub = subsets (lp)
        return sub + [ [a] + subset for subset in sub ] 

def proper_subsets(l):
    """ 
        Retourne l'ensemble de sous ensembles
        sous la forme d'une liste où les 
        sous ensembles sont triés par taille
        en supprimant l'ensemble vide et l'ensemble entier
    """
    if l == []:
        return []
    else:
        return subsets(l)[1:-1]

def vrai_simplexe(A,b,c):
    res = scopt.linprog(c=c, A_eq=A, b_eq=b, 
                        bounds=(0,None), method='simplex', callback=None,
                        options={ "maxiter" : 100000, "disp" : True})
    logging.debug ("Résultat de l'appel à simplexe : {}".format (res))
    return res

def in_cut(s,e):
    """
        Determine si l'arete e est dans la
        coupe s (definie par une liste de sommets)
    """

    if (e[0] in s) != (e[1] in s): # != === XOR
        return True
    else:
        return False


def primal_ajoute_coupe (n, e_s, w, A,b,c, graphe, coupe):
    """ 
        Voilà
        ne modifie pas w / e_s 
    """
    logging.debug ("AJOUTE COUPE PRIMAL {}".format (coupe)) 

    for ligne in A:
        ligne.append (0)

    nouvelle_ligne = [ int (in_cut (coupe, graphe.edges[e])) for e in e_s ] 
    nouvelle_ligne = nouvelle_ligne + [ 0 for x in nouvelle_ligne ] + [ 0 for j in range (len (w)) ] + [-1]
    
    logging.debug ("nouvelle ligne : {}".format (nouvelle_ligne))

    A.append (nouvelle_ligne)
    b.append (2)
    c.append (0)


def primal_ajoute_edge (n, e_s, w, A, b, c, graphe, edge_add):
    """ 
        Voilà 
        ne modifie pas w / e_s
    """
    logging.debug ("AJOUTE EDGE PRIMAL {}".format (edge_add)) 
    m = len (e_s)

    u,v = graphe.edges[edge_add]
    
    # ÉTAPE 1: trouver la position d'insertion 
    i = 0
    while i < m and e_s[i] < edge_add:
        i+=1
        
    # ÉTAPE 2: insérer dans la première partie
    # de la matrice les coefs pour 
    # l'égalité à deux
    for j in range (n):
        A[j].insert (i, int (j == u or j == v))
        A[j].insert (i + m + 1, 0)

    # ÉTAPE 3: insérer dans la deuxième partie 
    # de la matrice les coefs pour la variable 
    # et sa slack (note c'est l'identité)
    for j in range (m):
        A[n + j].insert (i, 0)
        A[n + j].insert (i + m + 1, 0)

    A.insert (n + i, [ int (j == i) for j in range (m+1) ] + [ int (j == i) for j in range (m+1) ])

    # ÉTAPE 4: modifier 
    # les contraintes de sous-tours (coupes)
    for j,S in enumerate (w):
        A[n + m + 1 + j].insert (i, int (in_cut (S,(u,v)))) 


    b.insert (n + i, 1)
    c.insert (i, graphe.distance (u,v))
    c.insert (i + m + 1, 0)

def dual_ajoute_coupe (n, e_s, w, A, b, c, graphe, S):
    """
        n   = taille de nu+ (nombre de sommets du graphe)
        e_s = liste triée par ordre croissant des indices 
              d'arêtes considérés (E')
        w   = liste des coupes considérées 
        A   = La matrice des contraintes 
        b   = le vecteur des contraintes 
        c   = le vecteur des couts 

        graphe = le graphe sur lequel on travaille 
        S   = la coupe qu'on ajoute 

        Modifie en place les trucs 
    """
    logging.debug ("AJOUTE COUPE DUAL {}".format (S)) 

    for edge_number_prim,ligne in enumerate(A):

        edge_number = e_s[edge_number_prim]
        edge = graphe.edges[edge_number]
        
        if in_cut (S, edge):
            ligne.append (1)
        else:
            ligne.append (0)

    w.append (S)

    c.append (2)


def dual_ajoute_edge (n, e_s, w, A, b, c, graphe, edge_add):
    """
        n   = taille de nu+ (nombre de sommets du graphe)
        e_s = liste triée par ordre croissant des indices 
              d'arêtes considérés (E')
        w   = liste des coupes considérées 
        A   = La matrice des contraintes 
        b   = le vecteur des contraintes 
        c   = le vecteur des couts 

        graphe = le graphe sur lequel on travaille 

        edge = l'arête qu'on ajoute, comme le 
            NUMÉRO ASSOCIÉ DANS LE GRAPHE !!
            (u,v) ---> graphe.edge_number (u,v)
    """

    logging.debug ("AJOUTE EDGE DUAL {}".format (edge_add)) 

    m = len (e_s)
    k = len (w)

    # ÉTAPE 1: trouver la position d'insertion 
    i = 0
    while i < m and e_s[i] < edge_add:
        i+=1

    for edge_number_prim,ligne in enumerate (A):

        edge_number = e_s[edge_number_prim]
        edge = graphe.edges[edge_number]
       
        # FIXME: check inutile ?
        if edge == edge_add:
            ligne.insert (2 * n + i, -1)
            ligne.insert (2 * n + m + 1 + i, 1)
        else:
            ligne.insert (2 * n + i, 0)
            ligne.insert (2 * n + m + 1 + i, 0)

    nouvelle_ligne = [ 0 for j in range (2 * n + 2 * (m+1)) ]

    (u,v) = graphe.edges[edge_add] 

    nouvelle_ligne[u]         =  1
    nouvelle_ligne[v]         =  1
    nouvelle_ligne[u + n]     = -1
    nouvelle_ligne[v + n]     = -1
    nouvelle_ligne[2 * n + i] = -1
    nouvelle_ligne[2 * n + m + 1 + i] = 1 

    nouvelle_ligne = nouvelle_ligne + [ int (in_cut (W[j], edge_add)) for j in range (k) ]


    e_s.insert (i,edge_add)
    A.insert (i, nouvelle_ligne)
    b.insert (i, -1 * graphe.distance (u,v))
    c.insert (2 * n + i, 0)
    c.insert (2 * n + m + 1 + i, -1)
    

def initialisation_dual (graphe):
    """
        Retourne les A,b,c,w,e_s,
        qui correspondent 
        à l'initialisation 
        du dual 

        (e_s,w,A,b,c)
    """

    logging.debug ("INITIALISE DUAL")

    nup = [ -2 for i in range (graphe._n) ]
    num = [  2 for i in range (graphe._n) ]

    return ([], [], [], [], nup + num)


def trouve_arete_a_ajouter (n,e_s,w,A,b,c,graphe,solution):
    m = len (e_s)


    def contrainte_viol (x):
        j,(u,v) = x

        x1 = graphe.distance (u,v)
        x2 = solution[u]
        x3 = solution[v]
        x4 = solution[u+n]
        x5 = solution[v+n]
        # x6 = solution[2 * n + j]
        # x7 = solution[2 * n + m + j]
        l  = [ solution[2 * n + 2 * m + j] for (j,S) in enumerate (w) if in_cut (S,(u,v)) ]
        x8 = sum (l)
        
        t = x1 + x2 + x3 - x4 - x5 + x8

        if abs (t) > 1:
            return abs (t)
        else:
            return None

    parcours = [ (j,u,v,contrainte_viol ((j,(u,v)))) for (j,(u,v)) in enumerate (graphe.edges) if j not in e_s ]

    parcours = [ (j,u,v,x) for (j,u,v,x) in parcours if x != None ] 

    if parcours == []:
        print ("Ne trouve pas d'edge à ajouter")
        return None
    
    (j,u,v,t) = max (parcours, key=lambda x: x[3])
    print ("Ajoute {} avec un cout {}".format (j, t)) 
    logging.debug ("EDGE_QUI_NE_VERIFIE_PAS {}".format (j))
    return (j,(u,v))
    



def algorithme_separation (graphe, filename="default_separation_dual", maxiter=10000):
    """
        Effectue l'algorithme de la question 5 
        c'est à dire en ajoutant peu à peu 
        les contraintes de sous-tours 
        
        retourne la liste des arêtes avec 
        une valeur strictement positive 
    """

    logging.debug ("SEPARATION INTELLIGENTE START")

    edges         = graphe.edges
    n             = graphe._n
    (e_s,w,A,b,c) = initialisation_dual (graphe)

    A1 = [ [] for j in range (n) ]
    b1 = [ 2  for j in range (n) ]
    c1 = []

    
    # SÉLECTION D'UN CHEMIN INITIAL 
    l = list (graphe.cities_number ()) 
    random.shuffle (l) # NOTE: modifie la liste en place
    l.append (l[0])    # C'est un cycle, on ajoute donc le premier élément à la fin

    chemin_initial = map (lambda e: graphe.edge_number (*e), zip (l,l[1:]))

    for j in chemin_initial:
        primal_ajoute_edge (n,e_s,w,A1,b1,c1,graphe,j)
        dual_ajoute_edge   (n,e_s,w,A,b,c,graphe,j)

    # for S in proper_subsets (list (range (n))):
        # primal_ajoute_coupe (n,e_s,w,A1,b1,c1,graphe,S)
        # dual_ajoute_coupe   (n,e_s,w,A,b,c,graphe,S)

    # A2,b2,c2 = pl.pl_full (graphe)
    
    # assert A1 == A2
    # assert b1 == b2
    # assert c1 == c2

    print ("Starting")

    iteration     = 0

    while iteration < maxiter: # boucle du dual 

        logging.debug ("EDGES: {}".format (e_s))
        logging.debug ("CUTS : {}".format (w))
        logging.debug ("A:\n{}".format (A))
        logging.debug ("b:\n{}".format (b))
        logging.debug ("c:\n{}".format (c))

        iteration += 1
        logging.debug ("Résolution du problème pour la {}-ème itération".format (iteration))


        # On se demande si on a une bonne solution ou non  
        # logging.debug ("Solve dual problem")
        # res2 = minim (A,b, [ - y for y in c ])
        res1 = vrai_simplexe (A1,b1,c1)
        res2 = vrai_simplexe (A,b, [ -y for y in c ])
        logging.debug ("Dual value : {}".format (-1 * res2["fun"]))

        print ("{} - {}".format (res1["fun"], -1 * res2["fun"]))

        logging.debug ("CHECK if good primal solution")
        edge_to_add = trouve_arete_a_ajouter (n,e_s,w,A,b,c,graphe, res2["x"])
        logging.debug ("EDGE to add : {}".format (edge_to_add))
        
        if edge_to_add != None:
            (j,(u,v)) = edge_to_add 
            primal_ajoute_edge (n,e_s,w,A1,b1,c1,graphe,j)
            dual_ajoute_edge   (n,e_s,w,A,b,c,graphe,j)

        else:
            logging.debug ("END !!!!!")
            break

    print ("PASSE AU PROBLÈME PRIMAL avec {} / {} edges".format (len (e_s), len (graphe.edges)))

    while iteration < maxiter:
        iteration += 1

        # Récupère la valeur et la solution 
        res = vrai_simplexe (A1,b1,c1)

        x = res["x"]

        sol = list(x[:len(edges)]) # seul le debut est important
        logging.debug ("Solution : {}".format (sol))

        # chemin = pl.convert_to_edges (graphe, sol)
        # graphe.draw(chem  = [u for (u,c) in chemin ], 
                    # poids = [c for (u,c) in chemin ], 
                    # filename="images/{}_separation_{}.png".format (filename, iteration))
        
        C = np.zeros((n,n))
        for (poids,edge) in zip (sol,edges):
            C[edge[0]][edge[1]] = poids
            C[edge[1]][edge[0]] = poids # par symetrie

        coupe_min,valeur_min = mincut.mincut (graphe, C)
        logging.debug ("Trouve une coupe {} de valeur {}".format (coupe_min, valeur_min))

        if valeur_min < 2 - 0.001:
            line = "\n\t".join ([ "({},{})".format (i,v) for (i,v) in enumerate (x[:len(edges)]) ])
            logging.debug ("Valeur avant l'ajout de contrainte :\n\t{}".format (line))
            logging.debug ("Ajoute une contrainte de sous-tours correspondant à {}".format (coupe_min))
            primal_ajoute_coupe (n,e_s,w,A1,b1,c1,graphe,coupe_min)
            dual_ajoute_coupe   (n,e_s,w,A,b,c,graphe,coupe_min)
        else:
            logging.warning ("FIN PHASE PRIMAL, itération numéro {}".format (iteration))
            break


if __name__ == '__main__':

    filename = "ch150"
    # initialisation du loggeur 
    logging.basicConfig(filename='dual_run.log',
                        level=logging.DEBUG,
                        format='%(asctime)s [%(levelname)s] :line %(lineno)d: (%(funcName)s) %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')

    probleme = read_tsp_file ("problemes/{}.tsp".format(filename))

    graphe  = probleme["GRAPH"]
    # graphe   = graph.random_graph (14)
    algorithme_separation (graphe, "dual" + filename)
