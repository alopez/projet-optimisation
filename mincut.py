
# Algorithme pour la min cut 
# Edmonds-Karp


## Fonction importante : mincut(graph,C)

from   graph import Graph
import math
import numpy as np
from   parser import read_tsp_file


             
def parcours_largeur(graph, C, s, t, flot):
    """
        Parcours en largeur pour la recherche de chemins
        augmentants
        On retient la liste des peres
        Si pas de chemin augmentant on renvoie la coupe min
        partant de s et excluant t associee au flot max

        @graph: un graphe avec n sommets, complet
        @C    : matrice n×n de poids positifs 
        @s    : entier inférieur à n 
        @t    : entier inférieur à n
        @flot : ?
    """
    # Initialisation
    n       = graph._n 
    ouverts = [s]
    visited = [s]
    pere    = [-1 for x in range(n)]
    pere[s] = -2

    M    = [ 0 for x in range(n) ]
    M[s] = math.inf
    
    # Boucle de parcours en largeur
    while ouverts != []: # tant qu'il reste quelque chose à lire 
        u = ouverts.pop (0)

        for v in graph.cities_number (): 
            if C[u][v] - flot[u][v] > 0 and pere[v] == -1:

                # non visite et flot ameliorable
                pere[v] = u

                M[v] = min(M[u], C[u][v] - flot[u][v])

                if v != t :
                    ouverts.append(v)
                    visited.append(v)

                else: # on a trouve u chemin augmentant
                    return (M[t], pere)

    return (None, visited) # plus de chemin augmentant


def mincutEdmondsKarp(graph, C, s, t):
    """ 
        Coupe min du graph graphe
        pondere POSTIVEMENT par C
        Coupe entre s et t
    """
    n     = graph._n        # taille du graphe
    value = 0               # valeur du flot
    flot  = np.zeros ((n,n)) # matrice du flot

    # Boucle de recherche de chemins augmentants
    while True:
        pathFlow, other = parcours_largeur(graph, C, s, t, flot)
        
        if pathFlow == None:
            # les visites forment une coupe min
            cut = other
            break
        # sinon c'etait la liste des peres
        pere = other
        
        value = value + pathFlow
        v = t
        while v != s:
            u = pere[v]
            # augmentation du flot
            flot[u][v] = flot[u][v] + pathFlow
            flot[v][u] = flot[v][u] + pathFlow
            v = u

    return (cut,value)

    

def mincut(graph, C):
    """
        Coupe minimum du graphe
        graph pondere par la matrice C
        
        Retourne un tuple (coupe_min, valeur_min)
        coupe_min sous forme d'une liste de sommets
    """
    
    assert graph._n == len(C) # matrice des valeurs

    # La coupe minimale c'est la coupe qui est minimale, on prend 
    # donc le minimum sur les coupes possibles 
    # en comparant les coupes via leur valeur
    return min ([ mincutEdmondsKarp (graph, C, c1, c2) for c1 in graph.cities_number () 
                                                       for c2 in graph.cities_number ()
                                                       if c1 > c2 ]
                , key = lambda x: x[1])
  
  
if __name__ == '__main__':
    # Choix du fichier
    probleme = read_tsp_file ("problemes/a0test.tsp")

    ### Exemple
    graphe = probleme["GRAPH"]
    C = [[0,1,0,0,0],
        [1,0,3,3,3],
        [0,3,0,3,3],
        [0,3,3,0,3],
        [0,3,3,3,0]]

    cut, value = mincut(graphe, C)
    print(value)

    for i in cut:
        print (i)

