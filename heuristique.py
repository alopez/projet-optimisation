#!/usr/bin/env python3

# Lopez Aliaume & Douéneau Gaëtan 

from   parser    import read_tsp_file
import city
import graph
from   itertools import permutations
import random

import logging


def heuristique (graph,l=None):
    """ 
        Heuristique naïve pour un graphe complet

        Part d'un cycle arbitraire (suite de noeuds),
        permute localement deux voisins tant que ça 
        augmente la valeur du chemin

        l liste des sommets du tour dans l'ordre 
        de parcours avec le premier élément 
        égal au dernier
    """
    if l == None:
        # Copie de la liste, pour la permuter après 
        l = list (graph.cities_number ()) 
        random.shuffle (l) # NOTE: modifie la liste en place
        l.append (l[0])    # C'est un cycle, on ajoute donc le premier élément à la fin

    # le chemin ressemble maintenant à 
    # u [intérieur] u 
    # 
    # on ne modifie que l'intérieur du cycle 
    # car une rotation ne change rien 
    
    cout     = graph.cout_chemin (l)

    ameliore = True
    while ameliore:
        ameliore = False 

        for i in range (1, len (l) - 1): # on regarde les paires intérieures
            for j in range (i+2, len (l) - 1): # on regarde les paires intérieures 
                # on copie le chemin, 
                # en permutant enusuite les 
                # deux valeurs concernées 
                logging.debug ("Inverse la partie entre {} et {}".format (i,j))
                p = l[:i]
                m = l[i:j]
                q = l[j:]
                m.reverse ()
                
                r = p + m + q
                logging.debug ("Solution proposée : {}".format (r))

                # On calcul le nouveau cout
                # s'il est meilleur ... 
                c = graph.cout_chemin (r)
                if c < cout:
                    logging.debug ("Améliore le coup")
                    ameliore = True 
                    cout = c 
                    l    = r
                    # on quitte la boucle for 
                    # pour revenir à la boucle while
                    break

                logging.debug ("Inverse la partie externe au segment {}-{}".format (i,j))

                m.reverse ()
                q = q[:-1]
                p.reverse ()
                q.reverse ()
                r = p + m + q
                r.append (r[0])

                logging.debug ("Solution proposée : {}".format (r))

                # On calcul le nouveau cout
                # s'il est meilleur ... 
                c = graph.cout_chemin (r)
                if c < cout:
                    logging.debug ("Améliore le coup")
                    ameliore = True 
                    cout = c 
                    l    = r
                    # on quitte la boucle for 
                    # pour revenir à la boucle while
                    break

    return l
    
    
## Tests

if __name__ == '__main__':
    # initialisation du loggeur 
    logging.basicConfig(filename='heuristique_run.log',
                        level=logging.DEBUG,
                        format='%(asctime)s [%(levelname)s] :line %(lineno)d: (%(funcName)s) %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    filename = "berlin52"
    probleme = read_tsp_file ("problemes/{}.tsp".format (filename))
    print ("lu")
    graphe = probleme["GRAPH"]
    print("graph")

    tour = heuristique (graphe)

    print("\n===========")
    print("TOUR OPTIMAL :", [c for c in tour])
    print("VALEUR :", graphe.cout_chemin(tour))
    print("===========\n")

    graphe.draw(chem=graphe.sommets_vers_aretes (tour),filename="images/{}_naif.png".format (filename))
        
