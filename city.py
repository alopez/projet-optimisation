from math  import pi, sqrt, cos, sin, acos
from numpy import around


###############################################
######## CLASSES FOR THE CITIES #########
###############################################



class Euc_2D:
    def __init__(self, x = None, y = None, number = None):
        self.x = x
        self.y = y
        self.number = number

    def distance(self, other):
        assert (type(other)==Euc_2D)
        xd = self.x - other.x
        yd = self.y - other.y
        return round(sqrt(xd**2 + yd**2))


class Euc_3D:
    def __init__(self, x = None, y = None, z = None, number = None):
        self.x = x
        self.y = y
        self.z = z
        self.number = number

    def distance(self, other):
        assert (type(other)==Euc_3D)
        xd = self.x - other.x
        yd = self.y - other.y
        zd = self.z - other.z
        return round(sqrt(xd**2 + yd**2 + zd**2))


class Max_2D:
    def __init__(self, x = None, y = None, number = None):
        self.x = x
        self.y = y
        self.number = number

    def distance(self, other):
        assert (type(other)==Max_2D)
        xd = self.x - other.x
        yd = self.y - other.y
        return max(round(abs(xd)), round(abs(yd)))


class Max_3D:
    def __init__(self, x = None, y = None, z = None, number = None):
        self.x = x
        self.y = y
        self.z = z
        self.number = number

    def distance(self, other):
        assert (type(other)==Max_3D)
        xd = self.x - other.x
        yd = self.y - other.y
        zd = self.z - other.z
        return max(round(abs(xd)), round(abs(yd)), round(abs(zd)))


class Man_2D:
    def __init__(self, x = None, y = None, number = None):
        self.x = x
        self.y = y
        self.number = number

    def distance(self, other):
        assert (type(other)==Man_2D)
        xd = self.x - other.x
        yd = self.y - other.y
        return round(abs(xd) + abs(yd))


class Man_3D:
    def __init__(self, x = None, y = None, z = None, number = None):
        self.x = x
        self.y = y
        self.z = z
        self.number = number

    def distance(self, other):
        assert (type(other)==Man_3D)
        xd = self.x - other.x
        yd = self.y - other.y
        zd = self.z - other.z
        return round(abs(xd) + abs(yd) + abs(zd))


class GeoCoord:
    def __init__(self, degrees = None, minutes = None, number = None):
        self.number = number
        if degrees < 0:
            (self.degrees, self.minutes) = (degrees, -1 * minutes)
        else:
            (self.degrees, self.minutes) = (degrees, minutes)

    def toRadians(self):
        return (self.degrees + self.minutes / 60) * pi / 180.0


class GeoCity:
    def __init__(self, lat = None, lon = None, number = None):
        self.lat = lat.toRadians()
        self.lon = lon.toRadians()
        self.number = number

    def distance(self, other):
        assert(type(other)==GeoCity)
        lat1, lon1 = self.lat, self.lon
        lat2, lon2 = other.lat, other.lon

        if lat1 == lat2 and lon1 == lon2:
            return 0
        else:
            q1       = cos( lon1 - lon2 )
            q2       = cos( lat1 - lat2 )
            q3       = cos( lat1 + lat2 )
            radius   = 6378.388
            # distance formula from the documentation
            distance = radius * acos (1/2 * ((1 + q1) * q2 - (1 - q1) * q3)) + 1
            return(int(distance)) # truncate (toward zero)

class Explicit_2D:
    def __init__(self, x = None, y = None, number = None):
        self.x = x
        self.y = y
        self.number = number

class Explicit_3D:
    def __init__(self, x = None, y = None, number = None):
        self.x = x
        self.y = y
        self.number = number
