from lineartools import invert
import numpy as np
import random
import time
import scipy.optimize as scopt
import cvxpy as cvx




def vrai_simplexe (A,b,c):
    AA = np.copy(np.array(A, dtype = float))
    bb = np.copy(np.array(b, dtype = float))
    cc = np.copy(np.array(c, dtype = float))
    res = scopt.linprog(c=cc, A_eq=AA, b_eq=bb, 
                        bounds=(0,None), method='simplex', callback=None,
                        options={ "maxiter" : 100000})
    # logging.debug ("Résultat de l'appel à simplexe : {}".format (res))
    return res
    
    
def simpl_cvx(A,b,c):
    
    m,n = A.shape
    assert(n == len(c) and (m==len(b)))
    
    x = cvx.Variable(n)
    objective = cvx.Minimize(cvx.sum_entries(c*x))
    
    constraints = [0 <= x, A*x == b]
    
    prob = cvx.Problem(objective, constraints)
    opt = prob.solve()
    s = prob.status
    
    return({"status":s})
    

def testinv():

    sumnp = 0
    sumlin = 0

    for i in range(100):
        n = 100
        M = np.random.rand(n,n)
        print("Iteration",i)
        if(np.linalg.det(M) != 0):
            a =time.clock()
            np.linalg.inv(M)
            b = time.clock()
            sumnp += b-a
            print("numpy done")
            a =time.clock()
            invert(M)
            b = time.clock()
            sumlin += b-a
    print("=============")
    print("Numpy :",sumnp)
    print("Gauss :",sumlin)
    
    return
        
    
def testsimpl():
    
    sumnp = 0
    sumlin = 0
    
    sumopt = 0
    sumcvx = 0
    
    for i in range(1000):
        print("Iteration",i)
        m = 100
        n = 200
        
        A = np.random.rand(m,n)*10 - 5*np.ones((m,n))
        b = np.random.rand(m)*2
        c = np.random.rand(n)
        
        t =time.clock()
        res = vrai_simplexe(A,b,c)
        tt = time.clock()
        sumopt += tt - t
        print("Optimize",res["message"])
        
        t = time.clock()
        res = simpl_cvx(A,b,c)
        tt = time.clock()
        sumcvx += tt - t
        print("CVX",res["status"])
        
    print("=============")
    print("Optimize :",sumopt)
    print("CVX :",sumcvx)
    return
    
