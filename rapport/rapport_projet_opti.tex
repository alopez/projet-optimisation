\documentclass[a4paper,10pt]{article}
\usepackage[a4paper]{geometry}
\geometry{ left=2cm, right=2cm,top=2.5cm ,bottom=3cm}


\usepackage{palatino}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath,amssymb}%for \mod, \bmod, ..
\usepackage{amsthm,amsfonts,mathrsfs}
\usepackage[full,small]{complexity}
\usepackage{ stmaryrd }
\usepackage[french, onelanguage]{algorithm2e}

\usepackage{multirow}

\usepackage{ marvosym }

%pour le tableau récapitualif 
\usepackage{hyperref}

\usepackage{multicol}
\usepackage{enumerate}

\usepackage{tikz}
\usetikzlibrary{arrows,positioning,automata,shadows,shapes}


% Define block styles
\tikzstyle{decision} = [diamond, draw, fill=blue!20, 
    text width=4.5em, text badly centered, node distance=3cm, inner sep=0pt]
\tikzstyle{block} = [rectangle, draw, fill=blue!20, 
    text width=5em, text centered, rounded corners, minimum height=4em]
\tikzstyle{fichierbloc} = [rectangle, draw, 
    text width=10em, text centered, rounded corners, minimum height=3em]
\tikzstyle{line} = [draw, -latex']
\tikzstyle{cloud} = [draw, ellipse,fill=red!20, node distance=3cm,
    minimum height=2em]

\newtheorem{q}{Question}
\newtheorem{lemma}{Lemme}
\newtheorem{definition}{Définition}


\newcommand{\eqv}{\Leftrightarrow}
\newcommand{\ds}{\vspace{0.5\baselineskip}}

\title{\textbf{Le problème du voyageur de commerce} \\ 
Optimisation convexe \& combinatoire}
\author{\bsc{Douéneau} Gaëtan \and \bsc{Lopez} Aliaume}

\begin{document}
\maketitle



\begin{abstract}

Nous présentons dans ce rapport notre travail sur le problème du voyageur de commerce dans le cas d'un graphe où le coût des arêtes est euclidien.

\ds

Les points importants de notre projet sont les suivants :

\begin{itemize}

\item[]

\begin{itemize}

\item implémentation d'algorithme généraux : simplexe, coupe minimale, arbre couvrant de poids min ;

\item implémentation de plusieurs approximations (polynomiales) pour le TSP ;

\item description d'un problème linéaire associé au voyageur de commerce ;

\item résolution de ce PL par une méthode de séparation ;

\item comparaison primal-dual.

\end{itemize}

\end{itemize}

\ds

Des tests ont été menés, essentiellement sur les données de la banque TSPLIB, pour lesquelles nous avons développé un parser. Nous avons également créé quelques petites instances de format similaire.

\ds

L'ensemble du code python ainsi que les instances du problème tsp 
peuvent se trouver à l'adresse suivante~: \url{https://gitlab.crans.org/alopez/projet-optimisation}.

\end{abstract}

\section*{Introduction}

\subsection*{Organisation du programme}

Nous détaillerons au fur et à mesure des questions les fonctionnalités de notre projet. Pour une vision globale, on peut se rapporter à la Figure \ref{include}. Nous avons implémenté un parser pour les fichiers de TSPLIB. En outre tous nos fichiers prennent des graphes de la classe \textsf{Graph}, définie dans \textsf{graph.py}.

\begin{figure}[h]
\begin{center}
    \includegraphics[width=13cm]{test.pdf}
\end{center}

\caption{\label{include}Graphe d'inclusion des différents fichiers}

\end{figure}


\ds

\subsection*{Remarques générales}

Afin d'assurer la lisibilité des exemples, avons en outre développé un moyen d'afficher les résultats (lorsque l'instance est euclidienne). De belles images (\textsf{/images}) ont été créées tout au long des tests.

\textbf{Note : les images sont parfois trompeuses\dots} Sur des petites distances, il faut faire très attention aux arrondis imposés par TSPLIB. Ainsi, sur un carré de côté 1, la diagonale est aussi courte que le bord !

\ds

Les exemples de la TSPLIB sont très pertinents pour tester l'efficacité de nos algorithmes :

\begin{itemize}
\item ils correspondent à des instances réelles ;
\item leurs tailles et niveaux de difficulté sont variables ;
\item la distance la plus utilisée est l'euclidienne, ce qui permet de représenter facilement les solutions. Mais on peut aussi tester notre efficacité sur d'autres distances.
\end{itemize}

\ds

Dans chaque fichier auxiliaire, on peut tester ses fonctions avec des exemples généralement à la fin.

\section*{Question 1 : l'algorithme du simplexe}

Nous avons codé l'algorithme du simplexe dans le fichier \textsf{simplexe.py}.

\subsection*{Remarques sur le code}

Notre simplexe permet de résoudre un problème de la forme \textsf{minimiser} $c^T x$ \textsf{avec} $Ax = b$, $x \ge 0$. Il se décompose en deux fonctions :

\begin{itemize}

\item \textsf{simplexe\_base} qui cherche à optimiser un problème en prenant une base (admissible) en entrée ;

\item \textsf{simplexe} qui en prend pas de base en entrée. Il cherche donc d'abord si le problème est faisable (Phase 1), puis se met à l'optimisation (Phase 2).

\end{itemize}

\ds

Nous implémentons une seule règle de pivotage : la règle de Bland. Elle choisit les indices entrants et sortants les plus petits possibles, et empêche le simplexe de passer deux fois par la même base. D'autres règles, comme la règle lexicographique, garantissent la même propriété.

\subsection*{Calcul matriciel}

Le seul algorithme non-autorisé par l'énoncé et dont nous ayons besoin pour le simplexe (tel qu'il est implémenté) est l'inversion d'une matrice. Dans \textsf{lineartools.py}, nous utilisons l'algorithme de Gauss-Jordan pour inverser une matrice carrée, fonction \textsf{invert}.

\ds

On peut très bien utiliser ce code pour inverser une matrice dans le simplexe,
mais il est \emph{vraiment beaucoup} plus lent que \textsf{numpy.linalg.inv}
sur des grosses matrices. Pour des raisons pratiques, nous utiliserons donc
cette dernière bibliothèque plutôt que notre algorithme \emph{maison}.

\subsection*{Remarques sur l'utilisation}

La bibliothèque \textsf{scipy} propose la fonction
\textsf{scipy.optimize.linprog} codant le simplexe, ce qui nous permet de
vérifier la cohérence de nos résultats.

\ds

Cette fonction est beaucoup plus rapide que notre simplexe, et surtout elle
gère mieux les erreurs d'arrondi en interrompant les calculs au-delà d'une
certaine précision. Nous voulions permettre à notre simplexe de faire des
calculs sans erreurs avec des fractions (module \textsf{Fractions}),
malheureusement, le produit matriciel \textsf{numpy.dot} est incompatible avec
le module en question. Nous aurions donc dû le coder à la main, mais alors il
aurait été beaucoup trop lent. Donc nous avons abandonné les fractions.

\ds

On a choisi de proposer l'utilisation des 2 simplexes (dans
\textsf{separation.py} par exemple). Cependant, dès lors que le nombre de
variables ou de contraintes augmente, nous préférerons utiliser
\textsf{scipy.optimize.linprog}. On peut tester notre simplexe en temps
raisonnable sur de petites instances et sans les contraintes de sous-tour.

\section*{Question 2 : l'algorithme naïf}

Comme demandé, nous avons implémenté l'algorithme naïf exact en $n !$ dans le
fichier \textsf{algonaif.py}, fonction \textsf{algorithme\_naif}. Celui ci
compare les valeurs du tour pour toutes les permutations possibles de
l'ensemble des sommets, avant de renvoyer l'optimum.

Sans surprise, cet algorithme devient rapidement inutilisable quand le nombre
de sommets augmente (autour de 10 c'est déjà difficile). Il ne peut servir de
comparaison exacte que dans des petits cas.



\section*{Question 3 : une heuristique efficace}

De nombreuses heuristiques existent pour le voyageur de commerce, certaines polynomiales, d'autre pas. Nous nous sommes concentrés ici sur deux méthodes dont la combinaison donnait d'assez bon résultats.

\subsection*{2OPT}

Cette première méthode est celle indiquée dans l'énoncé. Intuitivement, elle détruit les "mauvais croisements" d'arêtes. 2OPT consiste à améliorer un tour de manière itérative.

\ds

On propose deux manières \emph{équivalentes} de voir l'étape d'amélioration d'un tour :
\begin{itemize}

\item En considérant le tour comme un ensemble d'arêtes. On cherche alors à supprimer deux arêtes $(v_1,v_2)$ et $(v'_1,v'_2)$ du tour et les remplacer par $(v_1,v'_2)$ et $(v'_1,v_2)$, de sorte que la valeur du tour diminue.

\item En considérant le tour comme une permutation des sommets. On cherche alors à retourner un bloc de la permutation (vue \emph{sans bords} : la fin rejoint le début) tel que la valeur du tour diminue. \emph{C'est cette dernière implémentation que nous avons choisie}.

\end{itemize}

\ds

Il faut donner un tour de départ à cet algorithme. Dans un premier temps nous lui fournissons une permutation aléatoire des sommets. Implémenté dans le fichier \textsf{heuristique.py}, fonction \textsf{heuristique}.

\ds 
Notons que les résultats de cet algorithme aucune garantie \emph{théorique} sur l'écart à l'optimum.

\ds

\textbf{Note :} A priori rien ne garantit non plus que cet algorithme ne fasse qu'un nombre polynomial d'itérations avant de ne plus pouvoir diminuer la valeur du tour. On peut cependant pallier ce problème, par exemple en déterminant le nombre d'étapes maximales à exécuter (en fonction de la taille de la solution).


\subsection*{Approximation de ratio 2}

Exécuter un algorithme à ratio d'approximation constant permet de déduire \emph{un encadrement de l'optimum}, et pas seulement une borne supérieure. Cependant, peut montrer que si le problème du voyageur de commerce est approximable à ratio constant (dans le cas d'un graphe quelconque), alors $ \P = \NP$. Il ne faut donc pas s'attendre à trouver (facilement) une 2-approximation.

\ds

Heureusement, nous sommes ici dans un cas particulier : \emph{l'inégalité triangulaire est vérifiée} (l'énoncé nous le rappelle, et c'est le cas pour les instances de TSPLIB que nous traitons). Il existe alors un algorithme polynomial simple qui garantit un ratio d'approximation de $2$, basé sur le calcul d'un arbre couvrant de poids minimal puis un parcours en profondeur de celui-ci.


\ds

Implémenté dans le fichier \textsf{approximation2.py}, fonction  \textsf{approx2}.

\ds

Quelques bornes fournies par cet algorithme sont indiquées dans la Table \ref{approx} en Annexe. La comparaison avec l'optimal donné par TSPLIB assure que le ratio est bien 2.

\ds

\textbf{Note :} l'aide de l'algorithme d'Edmonds pour le couplage parfait de poids minimal, on pourrait construire un algorithme de $1.5$-approximation (toujours dans le cas de l'inégalité triangulaire). Nous ne l'avons pas implémenté ici.

\subsection*{Combinaison des deux techniques}

L'intuition que nous avions en programmant ces deux techniques était la suivante :

\begin{itemize}

\item le tour créé à partir d'un arbre couvrant de poids min est "globalement" faible, mais il peut faire "localement" de mauvais croisements (notamment avec les arcs de retour qui ne sont pas dans l'arbre) ;

\item les échanges de paires dans la 2OPT sont des modifications "locales". Typiquement, il peuvent corriger les mauvais croisement de l'approximation précédente.

\end{itemize}

\ds

Nous avons donc exécuté \emph{la 2OPT à partir du tour donné par la 2-approximation}. Le fichier \textsf{approximation.py} permet de tester les résultats en appelant les algorithmes précédents (en créant automatiquement des images).

\ds

Le ratio 2 est toujours garanti, mais en pratique nous obtenons de \emph{bien meilleures solutions}. 
Quelques résultats sont donnés dans la Table \ref{approx} en Annexe \ref{antable},
et sont très proches des valeurs optimales (parfois même on les atteint) !

Un exemple d'affichage est donné dans la Figure \ref{figapprox}, où l'on peut distinctement observer 
les erreurs «~locales~» de la 2-approximation qui proviennent des arcs de retours, puis 
les corrections effectuées par l'algorithme 2OPT.

\ds

\textbf{Note :} Le facteur limitant en temps dans cet algorithme est surtout 2OPT, qui est assez lent.

\ds
\begin{figure}[h]
\begin{center}

\begin{multicols}{2}
    \fbox{\includegraphics[width=7.5cm]{approx2_exemple.png}}
    
   Approximation à l'arbre couvrant seule
    
    
    \columnbreak
    
    \fbox{\includegraphics[width=7.5cm]{approx2_2opt_exemple.png}}
    
     Approximation à l'arbre couvrant + 2OPT
    
  
    
    \end{multicols}
        \caption{\label{figapprox} Deux approximations sur le problème \textsf{gil262}.}
\end{center}

\end{figure}


\section*{Question 4 : Programme linéaire simple}

On code les algorithmes qui écriront les problèmes linéaires, fichier \textsf{programmelineaire.py} :

\begin{itemize}

\item fonction \textsf{pl\_simple} : crée un programme linéaire à partir d'un graphe, sans les contraintes de sous-tour ;

\item fonction \textsf{pl\_full} : crée un programme linéaire à partir d'un graphe, avec les contraintes de sous-tour.

\end{itemize}

\ds

Ces fonctions codent le TSP en PL(NE) de la forme \textsf{minimiser} $c^T x$ \textsf{avec} $Ax = b$, $x \ge 0$ (pour pouvoir le donner facilement à manger à un simplexe). Le codage choisi est détaillé dans l'annexe \ref{ancodage} (dans le cas général où l'on ne prend que certaines arêtes et certaines coupes).

\ds

Le fichier \textsf{testpl.py} permet de regarder quelques résultats :

\begin{itemize}

\item fonction \textsf{test\_pl\_simple\_on\_file} : test du PL sans contraintes de sous-tour sur un fichier ;

\item fonction \textsf{test\_pl\_full\_on\_file} : test du PL avec contraintes de sous-tour sur un fichier (il faut prendre une petite instance, typiquement celles que nous avons créées : \textsf{a0test.tsp});

\item fonction \textsf{test\_pl\_simple\_on\_random\_graph} : test du PL sans contraintes de sous-tour sur un petit graphe généré aléatoirement ;

\item fonction \textsf{test\_pl\_full\_on\_random\_graph} : test du PL avec contraintes de sous-tour sur un petit graphe généré aléatoirement.
\end{itemize}

\ds

Notons que le nombre exponentiel de contraintes de sous-tour rend le \emph{travail très difficile pour le simplexe}. En effet, même \textsf{scipy.optimize.linprog} ne parvient pas à trouver de point faisable pour le PL full associé à notre test \textsf{a0valise.tsp} qui possède seulement 9 points ! Nous sommes sujets à des erreurs numériques, qui augmentent quand le nombre de contraintes augmente. D'autre part, sans contraintes de sous-tour, le simplexe est quand même trop lent si la taille des données devient grosse.

\ds

En ce qui concerne l'affichage, on dessine les arêtes en niveaux de gris (en fonction de leur poids dans la solution). Notons que cela ne donne pas tout le temps un tour (cf question suivante).



\section*{Question théorique 1 : intégralité du problème ?}

On se demande si le problème linéaire avec contraintes de sous-tour est entier.

\subsection*{Le problème n'est pas entier}

Notons que le calcul de la coupe minimale fournit un \emph{oracle de séparation polynomial} pour ce problème (cf Question 5). En utilisant la méthode de l'ellipsoïde, on pourrait donc avoir un algorithme en temps polynomial pour résoudre le problème linéaire.

Si le problème était entier, on aurait donc un algorithme en temps polynomial pour résoudre le voyageur de commerce ! Notre intuition est donc la suivante : \emph{le problème avec contraintes de sous-tour n'est pas entier}.

\ds
Plutôt que de chercher un contre-exemple à la main, on tire aléatoirement des graphes à 6 sommets (avec distance euclidienne) et on résout le problème linéaire avec contraintes de sous-tour associé.

\ds

Sur la Figure \ref{contrex}, on a \emph{représenté un contre-exemple obtenu par recherche aléatoire}. Le niveau de gris indique le poids de chaque arête dans la solution. Pour justifier proprement que le problème n'est pas entier, il suffit de regarder la valeur optimale associée : elle vaut $20.625$. On ne peut clairement pas la réaliser avec un vecteur entier, puisque les coefficients aussi sont entiers.

\begin{figure}[h]
\begin{center}
    \fbox{\includegraphics[width=10cm]{non_entier_exemple.png}}
\end{center}

\caption{ \label{contrex} Contre-exemple pour l'intégralité. Le niveau de gris représente le poids dans la solution.}

\end{figure}


\subsection*{Que faire quand la solution n'est pas entière ?}

Si la solution renvoyée par le simplexe est entière, c'est aussi un tour optimal et on est content.

\ds

Quand la solution n'est pas entière, le PL avec contraintes de sous-tour ne permet pas de reconstituer un tour directement. La valeur optimale $v_f^*$ qu'il donne est une \emph{borne inférieure} de l'optimum du TSP notée $t$ (car le problème en nombres entiers est inclus dans sa relaxation). Donc $v_f^* \le t$.

\ds

Si l'on note $v_s^*$ la valeur optimale du PL sans contraintes de sous-tour, on a (mêmes raisons) $v_s^* \le v_f^* \le t$.

\ds

On voudrait tout de même créer un tour à partir de la solution, ce qui donnera une borne supérieure. On peut appliquer l'heuristique suivante : il faut prendre les arêtes qui ont un poids important dans la solution.

Etant donnée une solution au PL avec contraintes de sous-tour, on trie donc les arêtes en fonction de leur poids, puis on construit un tour de manière gloutonne (en ajoutant les arêtes dans l'ordre, si possible). C'est ce que fait la fonction \textsf{convert\_to\_circuit}, dans le fichier \textsf{programmelineaire.py}.

\ds

Si l'on note $a_t$ la valeur de ce tour, on a un encadrement du tour optimal
$t$ : $v_s^* \le v_f^* \le t \le a_t$. Nous avons indiqué dans la Table
\ref{plinf} en Annexe \ref{antable} quelques valeurs de $v_s^*$ (bornes
inférieures fournies par la solution d'un PL sans contraintes de sous-tour). Ce
sont de \emph{vraiment bonnes bornes} !

\section*{Question 5 : Oracle de séparation polynomial}

Nous avons implémenté la coupe minimale d'un graphe pondéré, fichier \textsf{mincut.py}, fonction \textsf{mincut}.

\ds
Ensuite on utilise cette coupe comme oracle de séparation (polynomial), dans le fichier \textsf{separation.py}. La fonction \textsf{algorithme\_separation} implémente exactement l'algorithme demandé par l'énoncé, dont le principe est résumé en Figure \ref{separ}.


\textbf{Note :} pour des raisons d'erreurs numériques du simplexe, nous décrétons qu'une contrainte n'est pas respectée quand la coupe minimale est $< 2 - \varepsilon$ et non pas $2$.

\begin{figure}[h]

    \begin{center}
        \begin{tikzpicture}[auto]
            \node [block] (simplex1) {Simplexe};
            \node [block,    right = of simplex1] (updatetour) {Ajoute la contrainte};
            \node [decision, below = 1cm of simplex1] (tour) {Contrainte violée ?};
            \node [block,    below = of tour] (fin) { Accepte };


            \path [line] (tour) -| node [near start] {oui} (updatetour);
            \path [line] (updatetour) -- (simplex1);
            \path [line] (simplex1) -- (tour);

            \path [line] (tour) -- node {non} (fin);

        \end{tikzpicture}
    \end{center}
    \caption{\label{separ}Boucle de l'algorithme de séparation}
    \label{org:boucle}
\end{figure}


\ds

On peut tester cet algorithme dans \textsf{testpl.py} avec les fonctions \textsf{test\_separation\_on\_random\_graph} ainsi que \textsf{test\_separation\_on\_file}. Un exemple simple permet de se rendre compte de l'\emph{efficacité de l'algorithme}. Pour un graphe complet à 9 points comme \textsf{a0valise}, le simplexe est incapable de résoudre le problème avec contraintes de sous-tours, en revanche la séparation renvoie un résultat quasi-instantanément.

\ds

En guise d'illustration, la Figure \ref{illustrate} représente les solutions successives obtenues par l'algorithme de séparation. Comme elles ne sont a priori pas entières, on représente les poids en nuances de gris.

\begin{figure}[h]
\begin{center}
        \includegraphics[width=3cm]{separation/separation_random_graph_separation_1.png}
        \hspace{1em}\vrule\hspace{1em}
        \includegraphics[width=3cm]{separation/separation_random_graph_separation_2.png}
        \hspace{1em}\vrule\hspace{1em}
        \includegraphics[width=3cm]{separation/separation_random_graph_separation_3.png}
        \hspace{1em}\vrule\hspace{1em}
        \includegraphics[width=3cm]{separation/separation_random_graph_separation_4.png}
    \end{center}
    \caption{\label{illustrate} Itérations de l'algorithme de séparation sur un graphe}
    \label{fig:iterationseparation}
\end{figure}

\ds

Faisons la remarque suivante : l'algorithme de séparation calcule une suite
$v^*_1 \dots v^*_n$ de valeurs telles que $v_s^* = v^*_1 \le v^*_2 \le \dots
\le v^*_n = v_f^* \le t$ (avec les notations de la Question précédente). En
effet on ajoute des contraintes à chaque étape, jusqu'à ce que tout soit
vérifié. Donc interrompre l'algorithme de séparation avant qu'il termine
\emph{offre aussi une borne inférieure}, qui peut-être meilleure que $v_s^*$.

\ds

Dans la Table \ref{tabenc}, nous présentons quelques bornes obtenues grâce à la séparation. Certaines coïncident avec l'optimum, sans surprise !

\ds

\textbf{Note :} En sélectionnant une contrainte qui est 
violée par la solution optimale actuelle, 
on ne peut pas garantir que le nouvel optimum augmentera strictement, 
mais on peut garantir que la solution du nouveau problème ne sera pas identique. C'est 
en réalité une heuristique pour tenter de faire augmenter le plus vite possible la valeur de l'optimum.



\section*{Question 6~: Écrire le problème dual} 

On exprime de manière générique le problème 
dual en ayant restreint à un ensemble $E'$ d'arêtes 
et un ensemble $W$ de coupes. Le primal s'écrit toujours de la même manière (voir l'équation \ref{eqn:primal} que l'on peut trouver page \pageref{eqn:primal}).
Le Lagrangien associé s'écrit~:

\begin{align*} 
    L(x,\lambda_1, \lambda_2, \lambda_3,\nu)
    =& 
    \sum_{uv \in E'} c_{uv}x_{uv} \\
    &- \sum_{uv \in E'} \lambda_{1,uv} x_{uv} \\
    &+ \sum_{uv \in E'} \lambda_{2,uv} (x_{uv} - 1) \\
    &+ \sum_{u \in V} \nu_u \left( \sum_{uv \in E'} - 2 \right) \\
    &+ \sum_{S \in W} \lambda_{3,S} \left( 2 - \sum_{uv \in E' \wedge uv \in \delta (S) } x_{uv} \right) \\
\end{align*}

Afin de simplifier l'écriture du dual on introduit deux matrices $D_{E'}$ et $G_{E'}$ comme suit~:

\begin{equation*}
    (D_{E'})_{u,vw} = \begin{cases} 1 & \textrm{ si } u = v \vee u = w \\ 0 & \textrm{ sinon } \end{cases}
\end{equation*}

\begin{equation*}
    (G_{E'})_{S,uv} = \begin{cases} 1 & \textrm{ si } uv \in \delta(S) \\ 0 & \textrm{ sinon } \end{cases}
\end{equation*}

En réalité, la matrice $D_{E'}$ est simplement la matrice d'incidence du graphe restreint aux arêtes $E'$.
L'expression du dual est alors simplement~:

\begin{equation} \label{eqn:dual}
    \boxed{
    \begin{array}{rll}
        \textrm{max} &  \displaystyle- \sum_{uv \in E'} \lambda_{1,uv} - 2 \sum_{u \in V} \nu_u + 2 \sum_{S \in W} \lambda_{3,S} \\
                     & \lambda_1 \geq 0 \\
                     & \lambda_2 \geq 0 \\
                     & \lambda_3 \geq 0 \\
                     & c_{E'} - \lambda_1 + \lambda_2 + {}^t D_{E'} \nu - {}^t G_{E'} \lambda_3 = 0
    \end{array}
    }
\end{equation}

Une expression du problème dual plus adaptée à l'algorithme du simplexe est donnée 
dans l'Annexe \ref{codage:dual}, ainsi que toutes les propriétés spécifiques au codage. 

\section*{Question 7~: Implémenter l'algorithme itératif de résolution du dual restreint}

Nous avions déjà vu précédemment que 
la méthode qui limite le nombre de contraintes de sous-tours 
est utile pour avoir des bornes inférieures (c'était $v^*_1 \dots v^*_n$). 
Cependant, même en ne prenant aucune contrainte de sous-tour, il devient 
assez long de résoudre le problème primal sur des instances 
de taille raisonnable comme \textsf{berlin53} ou \textsf{bier127}.

\ds

Une autre méthode pour diminuer de manière drastique le temps de calcul 
est de se restreindre à un sous ensemble $E'$ d'arêtes (tel
que le problème soit encore faisable).

\paragraph{ }
\textbf{Point clef~:} Comme pour l'algorithme de séparation du primal, il faut étudier
l'effet des contraintes sur la valeur de l'optimum du dual.
Supposons que $E_1 \subseteq E_2$, alors le programme dual à un optimum plus petit pour $E_1$, en effet, on a ajouté des contraintes :

\[
    \operatorname{OptDual}(W,E_2) \leq \operatorname{OptDual}(W,E_1) 
\]

Si on note $t$ la valeur optimale \emph{entière} avec toutes les contraintes, on 
peut alors avoir les encadrements suivants~:

\[
    \begin{cases}
        \operatorname{OptPrimal}(W,E) \leq t & \textrm{ cf. algorithme de séparation } \\
        \operatorname{OptPrimal}(W,E) = \operatorname{OptDual}(W,E) \leq \operatorname{OptDual}(W,E')   & \textrm{ cf. dualité forte }
    \end{cases}
\]

On ne peut en revanche rien déduire a priori sur $\operatorname{OptPrimal}(W,E')$ et $t$.

\paragraph{ } 

Si on considère une suite croissante d'ensemble d'arêtes, on a une suite 
décroissante de valeur du maximum pour le programme linéaire dual. L'idée 
est d'ajouter assez d'arêtes pour que l'optimum du programme linéaire dual 
soit identique à celui du programme linéaire dual avec toutes les arêtes~: on 
aurait ainsi trouvé une borne inférieure pour $t$.

\paragraph{ }

Pour choisir quelle arête ajouter, on regarde  
quelles contraintes du dual avec toutes les arêtes ne sont pas vérifiées par 
la solution optimale actuelle. C'est encore une fois une heuristique~: en faisant cela il n'y a pas de garantie 
de faire strictement diminuer la valeur du dual, mais c'est un moyen efficace 
d'y parvenir globalement.

\ds

\section*{Conclusion}

Une approximation quelconque du TSP permet de donner facilement une borne supérieure sur la valeur de l'optimum. Néanmoins nous n'avons aucun moyen direct savoir si la solution renvoyée est la meilleure, ou plus généralement de quantifier l'écart à l'optimum.

\ds

Les algorithmes à ratio d'approximation constant résolvent ce problème en donnant directement un encadrement de la valeur optimale. Cependant, obtenir un ratio fin (proche de $1$) peut se révéler très difficile. C'est même parfois impossible !

\ds

Nous avons implémenté ici un moyen de calculer des \emph{bornes inférieures}, grâce à une relaxation linéaire du problème du voyageur de commerce. Elle permet de montrer que nous avons effectivement atteint l'optimum lors de certaines approximations (c'est le cas de \textsf{bruma14}). D'une manière plus générale, elle permet d'estimer l'écart à l'optimum.

\ds

Cette implémentation n'est pourtant pas parfaite, notamment en termes de passage à l'échelle. Le problème linéaire croît rapidement, et des erreurs numériques liées à la précision apparaissent alors. Un calcul efficace en nombres symboliques permettrait peut-être de contourner cette question.







\newpage

\hspace{-\parindent}{\Huge{\textbf{Annexes}}}

\appendix




\section{Programme linéaire primal~: explications sur le codage} \label{codage:primal}

\label{ancodage}

On exprime de manière générique le problème 
primal en ayant restreint à un ensemble $E'$ d'arêtes 
et un ensemble $W$ de coupes pour vérifier les contraintes de sous-tours~:

\begin{equation} \label{eqn:primal}
    \boxed{
    \begin{array}{rlc}
        \textrm{min} & \displaystyle \sum_{uv \in E'} c_{uv} x_{uv} & \\
                     & \displaystyle \forall uv \in E', &\displaystyle 0 \leq x_{uv} \leq 1 \\
                     & \displaystyle \forall u \in V, &\displaystyle  \sum_{uv \in E'} x_{uv} = 2 \\
                     & \displaystyle \forall S \in W, &\displaystyle  \sum_{uv \in E' \wedge uv \in \delta(S)} x_{uv} \geq 2
    \end{array}
    }
\end{equation}


On code le problème avec une matrice de la forme suivante, la matrice interne est $A$, le vecteur 
colonne sur la droite est $b$, le vecteur correspondant à la dernière ligne est $c$.

\begin{equation}
    \begin{array}{r|c|c|c||c}
                   & uv \in E' & u'v' \in E' \textrm{ (slack)} & S \in W \textrm{ (slack)} & b \\ \hline 
        t \in V    & t \in (uv)  & 0                  & 0                 & 2 \\ \hline
        xy \in E'  & I  & I                  & 0                 & 1 \\ \hline
        S' \in W   & (uv) \in S'   & 0                  & -I                & 2 \\ \hline \hline 
        c   & c_{uv} & 0 & 0 &  
    \end{array}
\end{equation}

On remarque que la diagonale de la matrice $A$ est composée de blocs identités, ainsi que de la matrice 
d'incidence du graphe.

On peut alors résoudre le problème dont les $|E'|$ premières variables correspondent à la 
solution attendue pour le problème primal~: 

\begin{equation}
    \begin{array}{rl}
        \textrm{min} & {}^t c x  \\
                     & Ax = b \\
                     & x \geq 0
    \end{array}
\end{equation}

On peut alors constater qu'il est facile de mettre à jour ce problème quand une arête ou 
une coupe est ajoutée, et que c'est bien une formulation admissible pour l'algorithme
du simplexe.


\section{Programme linéaire dual~: explications sur le codage} \label{codage:dual}

Le programme linéaire dual tel qu'il est exprimé dans l'équation \ref{eqn:dual}
n'est pas sous une forme standard pour utiliser l'algorithme du simplexe et on 
utilise la technique classique pour se ramener à des variables positives~:

\begin{equation*}
    \begin{array}{rll}
        \textrm{max} & - \sum_{uv \in E'} \lambda_{1,uv} x_{uv} - 2 \sum_{u \in V} (\nu_u^+ - \nu_u^-)  + 2 \sum_{S \in W} \lambda_{3,S} \\
                     & \lambda_1 \geq 0 \\
                     & \lambda_2 \geq 0 \\
                     & \lambda_3 \geq 0 \\
                     & \nu^+ \geq 0 \\
                     & \nu^- \geq 0 \\
                     & c_{E'} - \lambda_1 + \lambda_2 + {}^t D_{E'} (\nu^+ - \nu^-) - {}^t G_{E'} \lambda_3 = 0
    \end{array}
\end{equation*}

Afin d'utiliser un algorithme, il faut s'accorder sur un encodage précis des contraintes et 
des variables dans les matrices et vecteurs qu'on donne à l'algorithme du simplexe. Pour cela, 
on code les variables comme suit~:

\begin{equation} \label{eqn:codevariable}
    \begin{pmatrix}
        \nu^+ & \nu^- & \lambda_1 & \lambda_2 & \lambda_3 \\ \hline 
        -2    & 2     & 0         & -1        &  +2  
    \end{pmatrix}
\end{equation}

Le vecteur d'égalité $b$ est alors un vecteur avec $|E'|$ composantes et 
qui vérifie~:

\begin{equation*}
    b_{uv} = - c_{uv}
\end{equation*}

On peut alors exprimer dans le tableau suivant la construction de la matrice $A$, du vecteur $b$ 
et du vecteur $c$ qui seront donnés en argument au simplexe en accord avec les notations précédentes~:

\begin{equation}
    \def\arraystretch{3}
    \begin{array}{r|c|c|c|c|c||c}
        & u_1 \in \nu^+ & u_2 \in \nu^- & u_1v_1 \in E' \, (\lambda_1) & u_1v_1 \in E' \, (\lambda_2) & S \in W \, (\lambda_3) & b \\ \hline
        uv \in E' & u_1 \in (uv) & (-1) \times (u_1 \in uv) & -I & I & uv \in S & -c_{uv} \\ \hline \hline 
        c & -2 & + 2 & 0 & - 1 & + 2 & 
    \end{array}
\end{equation}


\section{Tables de résultats sur TSPLIB}

\label{antable}

\begin{table}[h!]
    \begin{center}
        \begin{tabular}{l | c | c | c}
        Fichier & 2-approximation & 2-approx + 2OPT & Tour optimal (cf TSPLIB) \\ \hline \hline
a280 & $3446$ & 2829 & $2579$ \\ \hline

ali535 & $271084$ & $223804$ & $202310$ \\ \hline

berlin52 & $10114$ &$7755$ & $7542$ \\ \hline

bier127 & $157507$ & $127640$ & $118282$ \\ \hline

brd14051 & $655413$ && $[468942,469445]$ \\ \hline

burma14 & $3814$ & \textcolor{red}{$3323$} & $\textcolor{red}{3323}$ \\ \hline

ch130 & $8216$ & 6725 & $6110$ \\ \hline

ch150 & $8319$ &6774 & $6528$ \\ \hline

d198 & $18754$ & 16824 & $15780$ \\ \hline

d493 & $44949$ & 38323 & $35002$ \\ \hline

d657 & $66379$ && $48912$ \\ \hline

d1291& $70513$ && $50801$ \\ \hline

d2103 & $92758$ && $[79952,80450]$ \\ \hline

d15112 & $2182921$ && $[1564590,1573152]$ \\ \hline

eil51 & $592$ & 444 & $426$ \\ \hline

eil76 & $668$ & 587 & $538$ \\ \hline

eil101 & $895$ & 667 & $629$ \\ \hline

fl417 & $15444$ & 12774 & $11861$ \\ \hline

fl1577 & $29533$ && $[22204,22249]$ \\ \hline

fnl4461 & $252308$ && $182566$ \\ \hline

gil262 & $3376$ & $2662$ & $2378$ \\ \hline

gr96 & $75081$ & $62298$ & $55209$ \\ \hline

gr137 & $91542$ & $76707$ & $69853$ \\ \hline

gr202 & $51570$ & $42568$ & $40160$ \\ \hline

gr229 &$179546$ & $145413$ & $134602$ \\ \hline

gr431 & $227363$ & & $171414$ \\ \hline

gr666 & $396508$ && $294358$ \\ \hline

kroA100 & $27327$ & $22455$ & $21282$ \\ \hline

kroB100 & $25885$ & $23494$ & $22141$ \\ \hline

rl11849 & $1348261$ && $[920847,923368]$ \\ \hline

ulysses16 & $7903$ & $6875$ & $6859$ \\ \hline

ulysses22 & $8401$ & $7199$ & $7013$ \\ \hline


        \end{tabular}
    \end{center}
    \caption{\label{approx} Quelques bornes supérieures des heuristiques de la Question 3}
\end{table}

\begin{table}[h!]
    \begin{center}
        \begin{tabular}{l | c | c | c}
        Fichier & Borne inférieure $v_s^*$ & Tour optimal (cf TSPLIB) \\ \hline \hline
        
berlin52 & $7163$ & $7542$ \\ \hline

burma14 & $3001$ & $3323$ \\ \hline

eil51 & $416.5$ & $426$ \\ \hline

eil76 & $534$  & $538$ \\ \hline

pr76 & $98994.5$ & $108159$ \\ \hline

st70 & $623.5$ & $675$ \\ \hline

ulysses16 & $6113$ & $6859$ \\ \hline

        \end{tabular}
    \end{center}
    \caption{\label{plinf} Quelques bornes inférieures sans contraintes de sous-tours la Question 4}
\end{table}


\pagebreak
\begin{table}[h]
    \begin{center}
        \begin{tabular}{l | c | c | c}
        Fichier & Borne inférieure $v^*_f$ &Tour optimal $t$ (cf TSPLIB)\\ \hline \hline
       

burma14 & $\textcolor{red}{3323}$ &$\textcolor{red}{3323}$ \\ \hline

ulysses22 &\textcolor{red}{$7013$} & \textcolor{red}{$7013$}  \\ \hline

ulysses16 & \textcolor{red}{$6859$} & \textcolor{red}{$6859$}  \\ \hline

        \end{tabular}
    \end{center}
    \caption{\label{tabenc} Bornes inférieures obtenues a partir de la Question 6}
\end{table}

\pagebreak

\section{Avec un autre solveur : CVX + algo de Stoer-Wagner}

\begin{table}[h]
    \begin{center}
        \begin{tabular}{l | c | c | c | c}
        Fichier & Borne inférieure $v^*_f$ &Tour optimal $t$ (cf TSPLIB) & Borne supérieure $a_t$ + 2OPT & Itérations\\ \hline \hline
        
a0bigtest & $\textcolor{red}{4990}$ &&$\textcolor{red}{4990}$  & 9\\ \hline

a280 & $2566$ & $2579$ & $2718$  & 32\\ \hline

berlin52 & $\textcolor{red}{7542}$ &$\textcolor{red}{7542}$&$\textcolor{red}{7542}$ & 4 \\ \hline

bier127 & $117431$ &$118282$&$121194$ & 27 \\ \hline

burma14 & $\textcolor{red}{3323}$ &$\textcolor{red}{3323}$&$\textcolor{red}{3323}$ & 3 \\ \hline

ch130 & $6075$ &$6110$&$6307$ & 35 \\ \hline

ch150 & $6490$ &$6528$&$6680$ & 27  \\ \hline

eil51 & $422$ &$426$&$439$ & 9 \\ \hline

eil76 & $537$ &$538$&$553$ & 7 \\ \hline

eil101 & $627$ &$629$&$640$ & 29 \\ \hline

gr96 & $54577$ &$55209$&$56335$ & 21 \\ \hline

gr137 & $69096$ &$69853$&$70402$ & 19\\ \hline

kroA100 & $20936$ &$21282$&$22057$ & 19\\ \hline

kroB100 & $21834$ &$22141$&$23118$ & 29\\ \hline

kroC100 & $20472$ &$20749$&$21714$ & 20\\ \hline

kroD100 & $21141$ &$21294$&$21915$ &24 \\ \hline

kroE100 & $21799$ &$22068$&$22926$ & 22\\ \hline

kroA150 & $26299$ &$26524$&$27616$ & 21 \\ \hline

kroB150 & $25732$ &$26130$&$27034$ & 34 \\ \hline

lin105 & $14370$ &$14379$&$14438$ & 24 \\ \hline

pr76 & $105120$ &$108159$&$110140$ & 64 \\ \hline

pr107 & $\textcolor{red}{44303}$ &$\textcolor{red}{44303}$&$\textcolor{red}{44303}$ & 9 \\ \hline

pr124 & $58067$ &$59030$&$59567$ & 23 \\ \hline

pr136 & $95934$ &$96772$&$101636$ & 18 \\ \hline

pr144 & $58189$ &$58537$&$62879$ & 54 \\ \hline

pr152 & $73208$ &$73682$&$74961$ & 100 \\ \hline

rat99 & $1206$ &$1211$&$1255$ & 12 \\ \hline

rd100 & $7899$ &$7910$&$8048$ & 46 \\ \hline

st70 & $671$ &$675$&$697$ & 19 \\ \hline

ts225 & $115605$ &$126643$&$138954$ & 1  \\ \hline

ulysses16 & \textcolor{red}{$6859$}& $\textcolor{red}{6859}$& \textcolor{red}{$6859$} & 5 \\ \hline

ulysses22 &\textcolor{red}{$7013$} &$\textcolor{red}{7013}$& \textcolor{red}{$7013$}  & 8\\ \hline

u574 & 36714 & 36905 & 38572 & 125 \\ \hline 

        \end{tabular}
    \end{center}
    \caption{\label{tabenc} Encadrements obtenus a partir de la Question 5}
\end{table}




\end{document}

