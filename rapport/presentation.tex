\documentclass[a4paper,10pt]{beamer}

\usecolortheme{whale}
\usecolortheme{orchid}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}%for \mod, \bmod, ..
\usepackage{amsthm,amsfonts,mathrsfs,bbold}
\usepackage[full,small]{complexity}
\usepackage{ stmaryrd }
\usepackage{ marvosym }

\usepackage[caption=false]{subfig}

%pour le tableau récapitualif 
\usepackage{hyperref}

\usepackage{enumerate,multicol}

\usepackage{tikz}
\usetikzlibrary{arrows,positioning,automata,shadows}

\newcommand{\Nat}{{\mathbb{N}}}
\newcommand{\eqv}{\Leftrightarrow}
\newcommand{\ds}{\vspace{0.5\baselineskip}}
\newcommand{\es}{\vspace{1\baselineskip}}

% Plan des sections et numéros des sections
\AtBeginSection[]{
  \frame{ 
    \frametitle{Plan}   
    \tableofcontents[currentsection]  }}
\setbeamertemplate{section in toc}[sections numbered]

% Titre
\title{\textbf{Optimisation Combinatoire \& Convexe} \\ Le problème du voyageur de commerce}
\author{Lopez Aliaume \and Douéneau Gaëtan}

\begin{document}

\maketitle 

\begin{frame}
    \frametitle{Problème du voyageur de commerce (TSP)}
    \begin{block}{Définition : TSP}
        Soit $G = \langle V, E \rangle$ un graphe (complet) 
        et $w : V \rightarrow R_+ \cup\{+ \infty\}$ une fonction de coût des arêtes. On cherche à calculer~:

        \[
            \min \{ w(C) ~|~ C \textrm{ cycle hamiltonien de } G \}
        \]
    \end{block}
    
    \es
    
    On considèrera $G$ non-orienté (problème symétrique).
\end{frame}

\begin{frame}
    \frametitle{Propriétés générales du problème}
    
        \begin{block}{Complexité}
        Problème de décision associé \NP-complet.
    \end{block}
       \begin{alertblock}{Approximabilité}
    Inapproximable à ratio constant, sauf si \P = \NP.
    
        \end{alertblock}
    
    
\end{frame}


\begin{frame}
    \frametitle{Dans le cas euclidien ou métrique}
    
           \begin{alertblock}{Restricition}
    On ne considère que des instances où la fonction de coût vérifie l'inégalité triangulaire.
    
        \end{alertblock}
        
                   \begin{exampleblock}{Exemple}
    Cas de la distance euclidienne dans le plan. Facilement représentable.
    
    Mais le problème de décision associé reste \NP-complet.
    
        \end{exampleblock}
        
        \ds

   Bonne nouvelle : le problème métrique est approximable à ratio constant.
\end{frame}


\begin{frame}
    \frametitle{TSPLIB}

    \begin{block}{TSPLIB95}
        TSPLIB une librairie d'instances de TSP de valeurs optimales connues (ou tight encadrement).
    \end{block}

    \begin{exampleblock}{Différentes instances}
    
    \begin{itemize}
    
    \item taille variable, entre 10 et 10000 sommets
\item différentes fonctions de coût (distance euclidienne, géodésique\dots).

\item parfois empruntés à la vie réélle (circuits électroniques, cartes\dots)


\end{itemize}
    \end{exampleblock}
\end{frame}



\section{Approximation}

\begin{frame}
    \frametitle{L'algorithme 2-OPT}

    \begin{block}{Algorithme pour optimiser un tour}
        Tant qu'on peut trouver deux arêtes 
        du tour $(v_1,v_2)$ et $(v_1', v_2')$ telles que 
        le tour en les remplaçant par $(v_1, v_2')$ et $(v_1',v_2)$
        a une valeur inférieure, on effectue cette opération.
    \end{block}

    \begin{exampleblock}{Exemple}
        \begin{center}
            \includegraphics[height=2cm]{../images/18116.jpg}
        \end{center}
    \end{exampleblock}


    \begin{alertblock}{Temps}
        La complexité en temps dans le pire des cas est un problème ouvert.
        
        Cependant elle est raisonnable en pratique.
    \end{alertblock}
\end{frame}


\begin{frame}

\frametitle{Garantie d'approximation}
    
    \begin{block}{2-approximation}
    Basée sur un arbre couvrant de poids minimal. Complexité quadratique.
    
    \end{block}

    \begin{block}{1.5-approximation \cite{christofides1976}}
    Algorithme de Christofides : arbre couvrant de poids min + couplage de poids minimum.
    \end{block}
    \ds 
    C'est toujours le meilleur ratio connu \cite{karpinski2015}.
\end{frame}

\begin{frame}
    \frametitle{Combinaison des deux approches}

    \begin{exampleblock}{Exemple~: eil101 (valeur optimale : 629)}
        \begin{figure}
            \centering
            \subfloat[2-approximation seule (878)]{ 
                \includegraphics[width=4cm]{../images/00_APPROX_eil101_approx2.png}
            }
            \subfloat[Suivie de 2OPT (679)]{ 
                \includegraphics[width=4cm]{../images/00_APPROX_eil101_approx2_2opt.png}
            }
            \caption{Évolution de la solution}
        \end{figure}
    \end{exampleblock}
\end{frame}


\begin{frame}
    \frametitle{Un algorithme glouton générique}

    \begin{block}{Glouton générique}
        On se donne un ordre sur les arêtes, et les ajoute de manière gloutonne jusqu'à obtenir un tour.
    \end{block}

    \begin{exampleblock}{Exemples}
        \begin{itemize}
        \item choix naturel : trier les arêtes en fonction de leur coût
                
        \item on verra plus loin qu'on peut les trier en fonction de la solution d'un autre problème.
        \end{itemize}
    \end{exampleblock}

\end{frame}


\begin{frame}
    \frametitle{Un algorithme glouton générique}
    \begin{block}{Exécution de l'algorithme}
        \begin{figure}
            \centering
            \subfloat[Ajout d'arêtes formant des chemins]{
                \includegraphics[width=5cm]{../images/00154_APPROX_kroA100_glouton.png}
            }
            \subfloat[Résultat final : tour ]{ 
                \includegraphics[width=5cm]{../images/00_APPROX_kroA100_glouton.png}
            }
            \caption{Algorithme glouton en utilisant les distances}
        \end{figure}
    \end{block}
\end{frame}



\begin{frame}
\frametitle{Comparaison des méthodes}
    \begin{exampleblock}{Exemple~: berlin52 (52 points, valeur optimale à 7542)}
        \begin{figure}
            \centering
            \begin{overlayarea}{\linewidth}{4cm}
                \only<1>{
                    \centering
                    \subfloat[2-approximation (10114)]{
                        \includegraphics[width=4.5cm]{../images/00_APPROX_berlin52_approx2.png}
                    }
                    \subfloat[2-approx + 2-OPT (7755)]{
                        \includegraphics[width=4.5cm]{../images/00_APPROX_berlin52_approx2_2opt.png}
                    }
                }
                \only<2>{
                    \centering
                    \subfloat[glouton (9951)]{
                        \includegraphics[width=4.5cm]{../images/00_APPROX_berlin52_glouton.png}
                    }
                    \subfloat[glouton + 2-OPT (7657)]{
                        \includegraphics[width=4.5cm]{../images/00_APPROX_berlin52_glouton_2opt.png}
                    }
                }
            \end{overlayarea}
        \end{figure}
    
    \end{exampleblock}
\end{frame}





\begin{frame}
    \frametitle{Quelques résultats d'approximation}

    \begin{table}[h!]
\begin{center}
\scalebox{0.8}{
        \begin{tabular}{l || c | c | c | c}
        Fichier& Sup 2-approx & 2-approx + 2OPT & OPT & Inf 2-approx\\ \hline \hline
a280 & $3446$ & 2829 & $2579$  & 1723 \\ \hline

ali535 & $271084$ & $223804$ & $202310$ &135542 \\ \hline

berlin52 & $10114$ &$7755$ & $7542$ &5057\\ \hline

bier127 & $157507$ & $127640$ & $118282$ & 78754\\ \hline

brd14051 & $655413$ && $[468942,469445]$&327707 \\ \hline

burma14 & $3814$ & \textcolor{red}{$3323$} & $\textcolor{red}{3323}$ &1907\\ \hline

ch150 & $8319$ &6774 & $6528$&4160 \\ \hline

d493 & $44949$ & 38323 & $35002$& 22475\\ \hline

eil51 & $592$ & 444 & $426$&296 \\ \hline

eil76 & $668$ & 587 & $538$& 334\\ \hline

eil101 & $895$ & 667 & $629$&448 \\ \hline

fl417 & $15444$ & 12774 & $11861$& 7722\\ \hline

kroA100 & $27327$ & $22455$ & $21282$ &13664\\ \hline

kroB100 & $25885$ & $23494$ & $22141$ &12943\\ \hline

ulysses22 & $8401$ & $7199$ & $7013$& 4201 \\ \hline
\end{tabular}}
    \end{center}
    \caption{\label{approx} Quelques encadrements par les heuristiques}
\end{table}

\end{frame}


\begin{frame}
    \frametitle{Quelques résultats d'approximation}
    
    \begin{exampleblock}{Bornes supérieures en pratique}
    
    2-approx + 2-OPT $\longrightarrow$ tour de l'ordre $1.1 \times$ OPT
    
    \end{exampleblock}
    
    \es
    
    \begin{exampleblock}{Bornes inférieures en pratique}
    
    2-approx $\longrightarrow$ borne inférieure de l'ordre de $0.6 \times$ OPT
    
    \end{exampleblock}
    
    \ds
    
    C'est ce dernier point qu'on va surtout chercher à améliorer.

\end{frame}
















\section{Problème linéaire et bornes inférieures}


\begin{frame}
    \frametitle{Programme linéaire en nombres entiers}

    \begin{block}{PLNE associé}
        \begin{equation} \label{eqn:primal:ilp}
            \boxed{
            \begin{array}{rlc}
                \textrm{Minimiser} & \displaystyle \sum_{uv \in E} c_{uv} x_{uv} & \\
                            \textrm{Avec}     & \displaystyle \forall u \in V, &\displaystyle  \sum_{uv \in E} x_{uv} = 2 \\
                             & \onslide<2->\displaystyle \forall \varnothing \subsetneq S \subsetneq E, &\displaystyle  \sum_{uv \in \delta(S)} x_{uv} \geq 2 \\
                             \onslide<1->
                             & \displaystyle \forall uv \in E, & x_{uv} \in \{0,1\} 
            \end{array}
            }
        \end{equation}

    \end{block}
    
    \onslide<3->

    \begin{exampleblock}{Relaxation linéaire}
        Remplacer la dernière contrainte par $0 \leq x_{uv} \leq 1$, donne une \emph{borne inférieure} au TSP.
    \end{exampleblock}

\end{frame}

\begin{frame}
    \frametitle{Un encadrement de la solution}
    \begin{block}{Borne inférieure}
       Les bornes inférieures permet d'obtenir une \emph{garantie} sur la qualité d'une approximation, sans même connaître l'optimum.

       \[
            v^* \leq TOUR \leq APPROX
       \]
    \end{block}
    
    \ds
    
    Mais cela ne permet pas de construire directement un tour approché, en dépit d'une information précieuse : la solution du PL !
    \ds
    \onslide<2->
    
    \begin{alertblock}{Construire un tour à partir du PL}
       Utiliser la pondération donnée par le PL comme poids 
       pour l'algorithme glouton !
    \end{alertblock}
    \ds
    Combiné avec 2-OPT, donne de bons résultats.
\end{frame}

\begin{frame}
    \frametitle{Quelques résultats du PL}
    
        \begin{alertblock}{Nouvel encadrement}
    
    $v^* \le TOUR \le$ Approximation(PL + glouton + 2-OPT)
    
    \end{alertblock}
    
    \es
    
    \begin{exampleblock}{Bornes supérieures obtenues en pratique}
    
    PL+ glouton + 2-OPT $\longrightarrow$ tour de l'ordre $1.05~\times$ OPT
    
    \end{exampleblock}
    
    \ds
    
    \begin{exampleblock}{Bornes inférieures obtenues en pratique}
    
    PL  $\longrightarrow$ borne inférieure de l'ordre de $0.95~\times$ OPT
    
    \end{exampleblock}
    
    \ds
    
    C'est mieux que $1.1$ et surtout $0.6$ !

\end{frame}


\begin{frame}
    \frametitle{Le problème n'est pas entier}

    \begin{exampleblock}{Contre-exemple~: ts225 (optimum à 126643)}
        \begin{figure}
            \centering
            \subfloat[Optimum du PL($115605$)]{
                \includegraphics[width=4.5cm]{../images/0CVX_ts225_separationfinie.png}
            }
            \subfloat[Une approximation à $138954$]{ 
                \includegraphics[width=4.5cm]{../images/0CVX_ts225_approxPL.png}
            }
            \caption{Programme linéaire sur ts225}
        \end{figure}

    \end{exampleblock}

\end{frame}



\begin{frame}
    \frametitle{Oracle de séparation}
    \begin{alertblock}{Problème}
    
        Nombre exponentiel de contraintes\dots
    \end{alertblock}

    \begin{block}{Solution}
        N'ajouter les contraintes de sous-tour que quand elles sont violées.
        
        Idée : trouver un oracle de séparation.
    \end{block}
    
    \onslide<2->
    
    \begin{block}{Algorithme de séparation}
        \[
            v_1 \leq v_2 \leq \dots \leq v^* \leq \text{OPT\_TSP}
        \]
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Oracle de séparation}


    \begin{overlayarea}{\linewidth}{4cm}
        \only<1>{
            \begin{block}{Coupe minimale et séparation}
                \begin{center}
                    Contrainte violée $\iff$ MinCut $< 2$
                \end{center}
            \end{block}
        }

        \only<2>{
            \begin{alertblock}{Coupe minimale et séparation}
                \begin{center}
                    La coupe minimale donne la contrainte 
                    «~la plus violée~»
                \end{center}
            \end{alertblock}
        }
    \end{overlayarea}

    \begin{block}{Calculs}
        \begin{itemize}
            \item Calcul de la coupe minimale (en temps polynomial)
            \item Calcul de la contrainte violée à partir 
                de la coupe minimale (linéaire)
        \end{itemize}
    \end{block}

\end{frame}

\begin{frame}
    \frametitle{Oracle de séparation}
    \begin{exampleblock}{Exemple~:}
        \begin{figure}
            \centering 
            \begin{overlayarea}{\linewidth}{5cm}
                \only<1>{
                    \centering
                    \subfloat[Itération 1 (7337)]{
                        \includegraphics[width=4.5cm]{../images/0CVX_rd100_simple_1.png}
                    }
                }
                \only<2>{
                    \centering 
                    \subfloat[Itération 11 (7578)]{
                        \includegraphics[width=4.5cm]{../images/0CVX_rd100_simple_11.png}
                    }
                }
                \only<3>{
                    \centering 
                    \subfloat[Itération 21 (7867)]{
                        \includegraphics[width=4.5cm]{../images/0CVX_rd100_simple_21.png}
                    }
                }
                \only<4>{
                    \centering 
                    \subfloat[Itération 46 et fin (7899)]{ 
                        \includegraphics[width=4.5cm]{../images/0CVX_rd100_separationfinie.png}
                    }
                }
            \end{overlayarea}
            \caption{Programme linéaire sur rd100 (7910)}
        \end{figure}
    \end{exampleblock}
\end{frame}




\section{Outils associés}

\begin{frame}
\frametitle{Parsing de TSPLIB}

\es

    \begin{exampleblock}{Différentes données à parser}
        \begin{itemize}
            \item matrice explicite des coûts
            \item coordonnées des sommets dans le plan (distance euclidienne)
            \item distance géodésique
        \end{itemize}
    \end{exampleblock}
    
    \ds
    
      \begin{alertblock}{Représentation}
        Distance euclidienne dans le plan : dessin sur Pygame.
    \end{alertblock}
    
    
\end{frame}


\begin{frame}
\frametitle{Algèbre linéaire}


    \begin{block}{Inversion de matrice}
        Implémentation du pivot de Gauss.
    \end{block}
    
    \ds
    
  
    Sans surprise, de très mauvaises performances : $140$s pour inverser 100 matrices aléatoires (dimension $100 \times 100$).
    
    \es
    
        \begin{block}{Numpy}
        \texttt{numpy.linalg.inv}
    \end{block}
    
    \ds 
        $0.9$s pour inverser les mêmes 100 matrices\dots
\end{frame}



\begin{frame}

\frametitle{Simplexe}

\begin{block}{Implémentation}

\begin{itemize}

\item PL sous forme standard : Maximiser $c^Tx$ avec $Ax = b$, $x \ge 0$

\item règle de Bland pour le pivotage

\item résoudre un problème de Phase 1, et un autre en Phase 2

\end{itemize}

\end{block}

\ds

Mais ce n'est pas très rapide (inversion de matrice à chaque étape, notemment).

\end{frame}

\begin{frame}

\frametitle{Simplexe}
    \begin{block}{\texttt{scipy.optimize.linopt}}
        Implémentation en Python de l'algorithme du simplexe.
    \end{block}
    
    \ds
    
    $24$s pour répondre à 100 instances (200 contraintes, 100 variables).
    
    \es

    \begin{block}{\texttt{CVXPy}}
        Interface élégante de \texttt{CVXopt} associée à Python. Très efficace.
    \end{block}
    
    \ds
    
    $4$s pour les 100 instances. Peu d'erreurs de faisabilité.

\end{frame}

\begin{frame}

\frametitle{Coupe minimale}

\begin{block}{Implémentation "naïve"}

Recherche des coupes minimales entre tous les couples de sommets.

O($|V|^3|E|^2$) (avec Edmonds-Karp)

\end{block}

\ds

Des performances suffisantes pour un "mauvais" simplexe.

\es

\begin{block}{Stoer-Wagner}

Tout faire en même temps.
\ds


Complexité : O($|V|(|E| + |V| \log|V|)$).
\end{block}
\ds

Implémenté dans NetworkX.

\end{frame}

\section{Résultats obtenus}

\begin{frame}

\frametitle{Bornes inférieures précises sur de grosses instances}

    \begin{exampleblock}{Sur un problème avec 657 points}
        \begin{figure}
            \centering
            \subfloat[Solution du PL($48452$)]{ 
                \includegraphics[width=5cm]{../images/0CVX_d657_separationfinie.png}
            }
            \subfloat[Tour associé ($51181$)]{ 
                \includegraphics[width=5cm]{../images/0CVX_d657_approxPL.png}
            }
            \caption{Le problème d657 de valeur optimale $48912$}
            \label{fig:pl:d657}
        \end{figure}
    \end{exampleblock}



\end{frame}


\begin{frame}
    \frametitle{Exemple d'historique~: un problème insatisfaisant}
    \begin{exampleblock}{Sur un problème avec 225 points}
        \begin{figure}
            \centering
            \begin{overlayarea}{\linewidth}{4cm}
                \only<1>{\centering \includegraphics[width=7cm]{../images/0CVX_tsp225_simple_1.png} \\ Étape 1}
                \only<2>{\centering \includegraphics[width=7cm]{../images/0CVX_tsp225_simple_11.png} \\ Étape 11 }
                \only<3>{\centering \includegraphics[width=7cm]{../images/0CVX_tsp225_simple_21.png} \\ Étape 21}
                \only<4>{\centering \includegraphics[width=7cm]{../images/0CVX_tsp225_simple_31.png} \\ Étape 31}
                \only<5>{\centering \includegraphics[width=7cm]{../images/0CVX_tsp225_simple_41.png} \\ Étape 41}
                \only<6>{\centering \includegraphics[width=7cm]{../images/0CVX_tsp225_separationfinie.png} \\ Étape 46}
                \only<7>{\centering \includegraphics[width=7cm]{../images/0CVX_tsp225_approxPL.png} \\ Tour construit }
                \only<8>{\centering \includegraphics[width=7cm]{../images/00_APPROX_tsp225_approx2_2opt.png} \\ 2-Approximation }
                \only<9>{\centering \includegraphics[width=7cm]{../images/00_APPROX_tsp225_glouton_2opt.png} \\ Glouton }
                \only<10>{ \vspace{2cm} L'approximation du plus proche voisin donne le résultat optimal ... } 
            \end{overlayarea}
            \caption{Le problème tsp225 de valeur optimale $3919$}
        \end{figure}
    \end{exampleblock}
\end{frame}

\section{Dualité}

\begin{frame}

\frametitle{Problème dual}

\begin{equation} \label{eqn:dual}
    \boxed{
    \begin{array}{rll}
        \textrm{Maximiser} &  \displaystyle- \sum_{uv \in E'} \lambda_{1,uv}  - 2 \sum_{u \in V} \nu_u + 2 \sum_{S \in W} \lambda_{3,S} \\
                  \textrm{Avec}     & \lambda_1 \geq 0 \\
                     & \lambda_2 \geq 0 \\
                     & \lambda_3 \geq 0 \\
                     & c_{E'} - \lambda_1 + \lambda_2 + {}^t D_{E'} \nu - {}^t G_{E'} \lambda_3 = 0
    \end{array}
    }
\end{equation}

Avec $D_{E'}$ la matirce d'incidence du graphe restreint à $E'$, et $G_{E'}$ la matrice qui vérifie~:


\begin{equation*}
    (G_{E'})_{S,uv} = \begin{cases} 1 & \textrm{ si } uv \in \delta(S) \\ 0 & \textrm{ sinon } \end{cases}
\end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Borne inférieure avec le dual}

    \begin{block}{Algorithme}
        Tant qu'une arête viole une contrainte du dual, on ajoute 
        l'arête et les contraintes associées. 
    \end{block}

    En pratique~: on ajoute toutes les arêtes.

    \begin{exampleblock}{Algorithme dual suivi de primal}
        Boucle~:
            \begin{enumerate}
                \item Calcul d'un sous ensemble $E'$ d'arête qui donne 
                    une minoration avec la méthode du dual 

                \item Calcul d'une contrainte de \emph{sous-tour}
                    à ajouter via le calcul du primal
            \end{enumerate}
    \end{exampleblock}

\end{frame}

\section*{Conclusion}
\begin{frame}
    \frametitle{Conclusion}

    \begin{block}{Contraintes supplémentaires}
        Les contraintes supplémentaires c'est bien 
    \end{block}

    \begin{block}{Des résultats pertinents en pratique}
        Encadrement de l'optimum à $\pm 5 \%$
    \end{block}
    
        \begin{alertblock}{La faiblesse de Python}
            \begin{itemize}
                \item Typage dynamique
                \item Consommation mémoire ($\approx 1.5$GO)
            \end{itemize}
    \end{alertblock}
\end{frame}


\section*{Bibliographie}



\begin{frame}[allowframebreaks]

\frametitle{Bibliographie}

\bibliographystyle{apalike}
\bibliography{presentation}

\end{frame}




\end{document}
