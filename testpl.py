import numpy as np
from   parser import read_tsp_file
import city
import graph
import simplexe 
import logging
import mincut

import programmelineaire as pl

import scipy.optimize as scopt

import separation 

def test_separation_on_random_graph ():
    """ 
        Un test de l'algorithme 
        de séparation pour un graphe aléatoire
    """
    graphe   = graph.random_graph (16)
    chemin   = separation.algorithme_separation (graphe, "separation_random_graph")
    

def test_separation_on_file (filename):
    """ 
        Un test de l'algorithme 
        de séparation pour un fichier donné
    """
    probleme = read_tsp_file ("problemes/{}.tsp".format(filename))
    graphe  = probleme["GRAPH"]
    chemin   = separation.algorithme_separation (graphe, filename)
    
    #aretes_chemin = pl.convert_to_circuit(graphe,chemin)

    #c = [ u for ((u,v),p) in aretes_chemin ] 

    #print(graphe.cout_chemin(c))


def test_pl_full_on_random_graph ():
    """ 
        Un test de l'algorithme 
        PL sur un graphe aléatoire 
    """
    graphe   = graph.random_graph (7, 100)
    filename = "pl_full_random"
    m        = len (list (graphe.edges))
    logging.debug ("EDGES :\n\t{}".format ("\n\t".join ([ "{} -> {}".format (u,v) for (u,v) in graphe.edges ])))

    # Construction du problème linéaire 
    (A,b,c) = pl.pl_full (graphe)

    res = separation.simplexe_func (A,b,c)
    x = res["x"]

    logging.debug ("Sortie du simplexe : {}".format (x))

    if all (map (lambda u: u % 1 == u, x[:m])):
        logging.debug ("Le problème est entier")
    else:
        line = "\n\t".join ([ "({},{})".format (i,v) for (i,v) in enumerate (x[:m]) ])
        logging.warning ("Le problème n'est pas entier :\n\t{}".format (line))
        print ("PAS ENTIER")

    chemin = pl.convert_to_edges (graphe, x)

    graphe.draw(chem=[ u for (u,c) in chemin ], 
                poids = [c for (u,c) in chemin ], 
                filename="images/{}_full.png".format (filename))

def test_pl_simple_on_random_graph ():
    """ 
        Un test de l'algorithme 
        PL simple sur un graphe aléatoire 
    """
    graphe   = graph.random_graph (6, 10)

    filename = "pl_simple_random"
    m        = len (list (graphe.edges))

    logging.debug ("EDGES :\n\t{}".format ("\n\t".join ([ "{} -> {}".format (u,v) for (u,v) in graphe.edges ])))

    # Construction du problème linéaire 
    (A,b,c) = pl.pl_simple (graphe)

    res = separation.simplexe_func (A,b,c)
    x = res["x"]

    chemin = pl.convert_to_edges (graphe, x)
    graphe.draw(chem=[ u for (u,c) in chemin ], poids = [c for (u,c) in chemin ], filename="images/{}_simple.png".format (filename))

def test_pl_simple_on_file (filename):
    """ 
        Un test de l'algorithme 
        PL simple sur un fichier donné 
    """

    probleme = read_tsp_file ("problemes/{}.tsp".format(filename))

    graphe  = probleme["GRAPH"]

    # Construction du problème linéaire 
    (A,b,c) = pl.pl_simple (graphe)

    res = separation.simplexe_func (A,b,c)
    x = res["x"]

    chemin = pl.convert_to_edges (graphe, x)
    graphe.draw(chem=[ u for (u,c) in chemin ], poids = [c for (u,c) in chemin ], filename="images/{}_simple.png".format (filename))
    
    return

def test_pl_full_on_file (filename):
    """ 
        Un test de l'algorithme 
        PL full sur un fichier donné 
    """

    probleme = read_tsp_file ("problemes/{}.tsp".format(filename))

    graphe  = probleme["GRAPH"]

    # Construction du problème linéaire 
    (A,b,c) = pl.pl_full (graphe)

    res = separation.simplexe_func (A,b,c)
    print (res)
    x = res["x"]

    chemin = pl.convert_to_edges (graphe, x)
    graphe.draw(chem=[ u for (u,c) in chemin ], poids = [c for (u,c) in chemin ], filename="images/{}_full.png".format (filename))
    
    return

if __name__ == '__main__':
    # initialisation du loggeur 
    logging.basicConfig(filename='testpl_run.log',
                        level=logging.DEBUG,
                        format='%(asctime)s [%(levelname)s] :line %(lineno)d: (%(funcName)s) %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    

    # POUR UTILISER LE SIMPLEXE « maison » 
    # retirer le commentaire sur cette ligne:
    # simplexe.simplexe_func = simplexe.mon_simplexe
    # Par défaut, le simplexe utilisé est celui de python

    # Choix du fichier
    # test_pl_simple_on_file  ("eil76")
    # test_pl_full_on_file    ("a0test")
    
    test_separation_on_file ("berlin52")

    # test_pl_simple_on_random_graph  ()
    # test_pl_full_on_random_graph    ()
    # test_separation_on_random_graph ()


    
